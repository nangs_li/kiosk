package com.echoroaster.digital_reception

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle

import io.flutter.app.FlutterActivity
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    Thread.setDefaultUncaughtExceptionHandler(UnCaughtExceptionHandler(this))
    GeneratedPluginRegistrant.registerWith(this)
  }
}