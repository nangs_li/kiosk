package com.echoroaster.digital_reception

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler

class BootReceiver : BroadcastReceiver() {

  override fun onReceive(context: Context, intent: Intent) {
    val h = Handler()
    h.postDelayed(Runnable {
      val dialogIntent = Intent(context, MainActivity::class.java)
      dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
      context.startActivity(dialogIntent)
    }, 1000)
  }

  companion object {
    private val TAG = BootReceiver::class.java!!.getSimpleName()
  }
}