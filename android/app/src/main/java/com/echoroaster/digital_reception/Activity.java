package com.echoroaster.digital_reception;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo.*;
import com.brother.ptouch.sdk.*;

import com.echoroaster.digital_reception.common.Common;
import com.echoroaster.digital_reception.common.MsgDialog;
import com.echoroaster.digital_reception.common.MsgHandle;
import com.echoroaster.digital_reception.printprocess.BasePrint;
import com.echoroaster.digital_reception.printprocess.ImagePrint;
import com.echoroaster.digital_reception.printprocess.MultiImagePrint;

import android.os.Environment;
import android.os.Message;
import android.util.Log;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.bluetooth.*;
import android.util.Base64;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public class Activity extends FlutterActivity {
  private final int PERMISSION_WRITE_EXTERNAL_STORAGE = 10001;
  private static final String CHANNEL = "samples.flutter.dev/battery";
  static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
  private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
    @TargetApi(12)
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      if (ACTION_USB_PERMISSION.equals(action)) {
        synchronized (this) {
          if (intent.getBooleanExtra(
                  UsbManager.EXTRA_PERMISSION_GRANTED, false))
            Common.mUsbRequest = 1;
          else
            Common.mUsbRequest = 2;
        }
      }
    }
  };
  BasePrint myPrint = null;
  MsgHandle mHandle;
  MsgDialog mDialog;
  private ArrayList<String> mFiles = new ArrayList<String>();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (!isPermitWriteStorage()) {
      requestPermissions(
              new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
              PERMISSION_WRITE_EXTERNAL_STORAGE);
    }

    GeneratedPluginRegistrant.registerWith(this);

    new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
            new MethodCallHandler() {
              @Override
              public void onMethodCall(MethodCall call, Result result) {
                // Note: this method is invoked on the main thread.
                if (call.method.equals("setLabelContext")) {
                  String name = call.argument("name");
                  String company = call.argument("company");
                  String photo = call.argument("photo");
                  startPrinter(name,company,photo);
                  result.success(true);

                } else   if (call.method.equals("printLabel")) {
                  myPrint.print();
                  result.success(true);

                } else   if (call.method.equals("clearPrintFile")) {
                   mFiles.clear();
                  result.success(true);
                }else {
                  result.notImplemented();
                }
              }
            });

  }

  private int getBatteryLevel() {
    int batteryLevel = -1;
    if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
      BatteryManager batteryManager = (BatteryManager) getSystemService(BATTERY_SERVICE);
      batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
    } else {
      Intent intent = new ContextWrapper(getApplicationContext()).
              registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
      batteryLevel = (intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100) /
              intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
    }

    return batteryLevel;
  }

  private boolean isPermitWriteStorage() {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
              != PackageManager.PERMISSION_GRANTED) {
        return false;
      }
    }
    return true;
  }

  public void startPrinter(String name,String company,String photo) {
    
    // initialization for printing
    mDialog = new MsgDialog(this);
    mHandle = new MsgHandle(this, mDialog);
    myPrint = new ImagePrint(this, mHandle, mDialog);

//    final Printer printer = new Printer();
    final PrinterInfo printerInfo = myPrint.getPrinterInfo();
    // For Bluetooth:
//    printer.setBluetooth(getBluetoothAdapter());
//    printerInfo.port = PrinterInfo.Port.BLUETOOTH;
//    printerInfo.macAddress =  "E0:7D:EA:6D:EF:C8";

    print(name,company,photo,null,0);
    // for Net;
//    printerInfo.port = PrinterInfo.Port.NET;
//    printerInfo.ipAddress = "192.168.118.1";

    // for USB;
    printerInfo.port = PrinterInfo.Port.USB;
    //printerInfo.macAddress =  "E0:7D:EA:6D:EF:C8";


    String externalStorageDir =
            Environment.getExternalStorageDirectory().toString();
    Log.d("externalStorageDir:", externalStorageDir);

    printerInfo.orientation = Orientation.LANDSCAPE;
    // Print Settings
    printerInfo.printerModel = PrinterInfo.Model.QL_820NWB;
    printerInfo.labelNameIndex = LabelInfo.QL700.W62.ordinal();
    printerInfo.printMode = PrinterInfo.PrintMode.FIT_TO_PAGE;
    printerInfo.isAutoCut = true;
    printerInfo.isLabelEndCut = true;

//    mFiles.clear();
//    mFiles.add("/storage/emulated/0/Download/youtube.jpg");

    // set the printing data
    ((ImagePrint) myPrint).setFiles(mFiles);
    Log.d("connect library success", myPrint.toString());
//    Log.d("PrinterInfo ipAddress:", prinxterInfo.ipAddress);
 //   Log.d("PrinterInfo macAddress:", printerInfo.macAddress);
      
    if (!checkUSB())
      return;


    // For USB:
    // printerInfo.port = PrinterInfo.Port.USB;

    //return printer.toString();
  }

  @TargetApi(12)
  boolean checkUSB() {
    if (myPrint.getPrinterInfo().port != PrinterInfo.Port.USB) {
      return true;
    }
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
      Message msg = mHandle.obtainMessage(Common.MSG_WRONG_OS);
      mHandle.sendMessage(msg);
      return false;
    }
    UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
    UsbDevice usbDevice = myPrint.getUsbDevice(usbManager);
    if (usbDevice == null) {
      Message msg = mHandle.obtainMessage(Common.MSG_NO_USB);
      mHandle.sendMessage(msg);
      return false;
    }
    PendingIntent permissionIntent = PendingIntent.getBroadcast(this, 0,
            new Intent(ACTION_USB_PERMISSION), 0);
    registerReceiver(mUsbReceiver, new IntentFilter(ACTION_USB_PERMISSION));
    if (!usbManager.hasPermission(usbDevice)) {
      Common.mUsbRequest = 0;
      usbManager.requestPermission(usbDevice, permissionIntent);
    } else {
      Common.mUsbRequest = 1;
    }
    return true;
  }

  private void print(String name, String company, String photo,Context context,int index) {

    final Paint paint = new Paint();
    paint.setTextSize(45);
    paint.setFakeBoldText(true);
    paint.setColor(Color.BLACK);
    paint.setTextAlign(Paint.Align.LEFT);


    final Paint bpaint = new Paint();
    bpaint.setTextSize(40);
    bpaint.setColor(Color.BLACK);
    bpaint.setTextAlign(Paint.Align.LEFT);

    float baseline = -paint.ascent();
    int width = (int) (paint.measureText("HELLO") + 0.5f);
    int height = (int) (baseline + paint.descent() + 0.5f);
    Log.d("tag", "Context: " + context);

    final Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.sample).copy(Bitmap.Config.ARGB_8888, true);
   // final Bitmap bitmap = BitmapFactory.decodeFile(src).copy(Bitmap.Config.ARGB_8888, true);
    final Canvas canvas = new Canvas(bitmap);
//        canvas.drawRect(0,0,400,80,wpaint);
//        canvas.drawRect(0,500,400,100,wpaint);
    if (name != null && !name.isEmpty()) {
      if (name.length() > 12) {
        paint.setTextSize(numberX3(28));
      } else {
        paint.setTextSize(numberX3(45));
      }
      canvas.drawText(name, 20, numberX3(baseline + 30), paint);
    }
    if (company != null && !company.isEmpty()) {
      if (company.length() > 16) {
        bpaint.setTextSize(numberX3(22));
      } else {
        bpaint.setTextSize(numberX3(40));
      }
      canvas.drawText(company, 20, numberX3(baseline + 290 + 10), bpaint);
    }


    if (photo != null && !photo.isEmpty()) {

//      if (photo.contains("http")) {
//        PrintAsyncTask printAsyncTask = new PrintAsyncTask(canvas, photo, paint, bitmap,context, index);
//        printAsyncTask.execute();
//      } else {
        byte[] decodedString = Base64.decode(photo, Base64.DEFAULT);
        Bitmap bm = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//        Bitmap bm = BitmapFactory.decodeFile(photo);
//                Bitmap bm = BitmapFactory.decodeByteArray(Base64.decode(photo,Base64.DEFAULT),0,Base64.decode(photo,Base64.DEFAULT).length);

       Bitmap b = getCroppedBitmap(Bitmap.createScaledBitmap(bm,intX3(160),intX3(180),false));
        if (b != null) {
          canvas.drawBitmap(b,numberX3(400 + 20),numberX3(180),paint);
        }

        try {
          String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                  "/printPhoto";
          File dir = new File(file_path);
          if(!dir.exists())
            dir.mkdirs();
          String file_str = UUID.randomUUID().toString() + ".png";
          File file = new File(dir, file_str);
          FileOutputStream fOut = new FileOutputStream(file);

          bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
          fOut.flush();
          fOut.close();

          mFiles.add(file_path + "/" + file_str);
        } catch (IOException e) {
          e.printStackTrace();
        }

//      }

    } else {
      try {
        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/printPhoto";
        File dir = new File(file_path);
        if(!dir.exists())
          dir.mkdirs();
        String file_str = UUID.randomUUID().toString() + ".png";
        File file = new File(dir, file_str);
        FileOutputStream fOut = new FileOutputStream(file);

        bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        fOut.flush();
        fOut.close();

        mFiles.add(file_path + "/" + file_str);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    }

  public int intX3(float number) {

    double doubleNumber = (double)number;
    double doubleNumberX3 = doubleNumber * 3.2487;

    return (int)doubleNumberX3;
  }

    public float numberX3(float number) {

    double doubleNumber = (double)number;
    double doubleNumberX3 = doubleNumber * 3.2487;
    float  floatNumberX3 = (float)doubleNumberX3;

    return floatNumberX3;
  }

  public Bitmap getCroppedBitmap(Bitmap bitmap) {
    Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
            bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(output);

    final int color = 0xffffffff;
    final Paint paint = new Paint();
    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
    canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
            bitmap.getWidth() / 2, paint);
    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    canvas.drawBitmap(bitmap, rect, rect, paint);
    //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
    //return _bmp;
    return output;
  }

}
