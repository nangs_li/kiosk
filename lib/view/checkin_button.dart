import 'package:flutter/material.dart';
import '../utils.dart';

class CustomButton extends StatelessWidget {

  CustomButton({ this.resPath });
  final String resPath;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: const Color(0x00ffffff),
      child: Container(
        width: 350/ Dimension.getRatio(context),
        height: 350/ Dimension.getRatio(context),
        margin: const EdgeInsets.only(left: 20, right: 20),
        alignment: Alignment(0.0, 0.0),
        child: CustomButtonItem(title: "", resPath: resPath,),
      )
    );
  }
}

class CustomButtonItem extends StatelessWidget {

  CustomButtonItem({ this.title, this.resPath });
  final String title;
  final String resPath;

  @override
  Widget build(BuildContext contexdt) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Image.asset(
            resPath
        ),
      ],
    );
  }
}