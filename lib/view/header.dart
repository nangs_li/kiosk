import 'package:flutter/material.dart';
import '../main.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import '../utils.dart';


class Header extends StatefulWidget {

  Header({this.callback, this.isBack, this.debugCallback});

  final VoidCallback callback;
  final VoidCallback debugCallback;
  String isBack;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HeaderState(callback: callback, isBack: isBack, debugCallback: debugCallback);
  }

}


class _HeaderState extends State<Header> {

  final VoidCallback callback;
  final VoidCallback debugCallback;
  String isBack;
  bool isConnected = true;

  _HeaderState({this.callback, this.isBack, this.debugCallback});

  connectivity() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() {
          isConnected = true;
        });
      }
    } on SocketException catch (_) {
      setState(() {
        isConnected = false;
      });
    }
  }

  @override
  initState() {
    super.initState();
    connectivity();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                child: (isBack != null) ? Image.asset("back_button.png", width: Dimension.getFullWidth(context) * 2 / 9,) : Image.asset("menu_button.png", width: Dimension.getFullWidth(context) * 2 / 9),
                onTap: () {
                  SystemSound.play(SystemSoundType.click);
                  (callback == null) ?
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyApp()),
                  ) : callback();
                },
              ),
              GestureDetector(
                child: Container(
                  margin: EdgeInsets.only(right: 72/ Dimension.getRatio(context)),
                  child: Image.asset("logo_icon.png", width: Dimension.getFullWidth(context) * 1 / 9,),
                ),
                onDoubleTap: () {
                  if (debugCallback != null) {
                    debugCallback();
                  }
                },
              ),
            ],
          ),
          !isConnected ? Image.asset("no_network.png", width: Dimension.getFullWidth(context) *3/135,) : Container(),
        ],
      ),
    );
  }
}