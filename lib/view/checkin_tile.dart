import 'package:digital_reception/model/appointmentData.dart';
import 'package:flutter/material.dart';
import '../utils.dart';
import 'dart:convert';

class CheckInTile extends StatefulWidget {

  final AppointmentUserData guest;
  final VoidCallback callback;

  CheckInTile({this.guest, this.callback});

  @override
  CheckInState createState() => CheckInState(guest: guest, callback: callback);
}

class CheckInState extends State<CheckInTile> {
  bool flag = false;
  AppointmentUserData guest;
  VoidCallback callback;

  CheckInState({this.guest, this.callback});

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (guest == null) ? Container() : GestureDetector(
      child: Container(
        margin: EdgeInsets.only(top: 40 / Dimension.getRatio(context)),
        child: Stack(
          children: <Widget>[
            flag ? Image.asset("selected_guest.png") : Image.asset("select_guest.png"),
            Container(
              margin: EdgeInsets.only(top: 60.0 / Dimension.getRatio(context), left: 20 / Dimension.getRatio(context)),
              width: 75 / Dimension.getRatio(context),
              height: 75 / Dimension.getRatio(context),
              child: CircleAvatar(
                backgroundImage: (guest.guestIconEndpoint == null) ? (flag ? Image.asset("avatar_active.png", width: Dimension.getFullWidth(context) * 1/9,).image : Image.asset("avatar_inactive.png", width: Dimension.getFullWidth(context) * 1/9).image) :((guest.guestIconEndpoint.contains("http")) ? NetworkImage(guest.guestIconEndpoint) : Image.memory(base64Decode(guest.guestIconEndpoint)).image),
              ),
            ),
            Container(
                margin: EdgeInsets.only(top: 175.0 / Dimension.getRatio(context), left: 20 / Dimension.getRatio(context), right: 20 / Dimension.getRatio(context)),
                child: Text(guest.guestFirstName + " " + guest.guestLastName, style: TextStyle(fontWeight: FontWeight.bold),maxLines: 1,)
            ),
            Container(
                margin: EdgeInsets.only(top: 200.0 / Dimension.getRatio(context), left: 20 / Dimension.getRatio(context), right: 20 / Dimension.getRatio(context)),
                child: Text(guest.guestCompany, style: TextStyle(fontWeight: FontWeight.normal),maxLines: 1,)
            ),
          ],
        )
      ),
      onTap: () {
        callback();
        setState(() {
          flag = !flag;
        });
      },
    );
  }
}