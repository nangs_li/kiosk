import 'package:digital_reception/model/ServerApiManager.dart';
import 'package:digital_reception/model/appointmentData.dart';
import 'package:digital_reception/model/singletonShareData.dart';
import 'package:digital_reception/view/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'qraccess.dart';
import 'guest.dart';
import 'view/checkin_button.dart';
import 'package:flutter/foundation.dart';
import 'dart:convert' as convert;
import 'dart:async';
import 'dart:convert';
import 'photo_taking.dart';
import 'checkin_complete.dart';
import 'model/guestModel.dart';
import 'utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sentry/sentry.dart';
//import 'package:catcher/catcher_plugin.dart';
import 'package:flutter/services.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
  FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Digital Reception',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page',
        analytics: analytics,
        observer: observer,),
    );
  }
}

class MyHomePage extends StatefulWidget {

  final String title;
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  MyHomePage({Key key, this.title, this.analytics, this.observer}) : super(key: key);

  @override
  _MyHomePageState createState() {
    Log.analytics = analytics;
    Log.observer = observer;
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  int printerOn = 0;
  int printerClick = 0;
  int printCount = 300;

  int comingSoonFlag = -1;
  String repairUrl = "";

  int wakeAlarmFlag = -1;
  String offImageUrl = "";
  String scheduleStartTime = "";
  String scheduleEndTime = "";
  bool scheduleActive = true;
  bool appDownload = false;
  String mainAppUrl = "";
  String printerAppUrl = "";

  Timer _timer;
  bool isError = false;
  int countdown = 3;
  String mInputText = "";
  TextEditingController _textController;
  bool isDebug = false;
  String version;
  Timer timer;
  int textLength = 0;
  @override
  void initState() {
    super.initState();
    getVersion();
    SystemChrome.setEnabledSystemUIOverlays([]);
    _textController = new TextEditingController();
    initSharedPreferences();
    initFireStore();
    getVersion();
   // Log.sendAnalyticsEvent("test", "test value", "test action", "LOG");

    if (_timer != null) {
      _timer.cancel();
    }
    _timer = new Timer.periodic(const Duration(seconds: 3), setTime);

    data.showLoadingUI.listen((bool showLoading) {
      setState(() {
        data.isLoading = showLoading;
      });

    });
    timer = Timer.periodic(
        Duration(milliseconds: 1000), (timer) {
      data.checkQRCodeScanSuccess(textController: _textController,isEmployee: false,context: context);
    });
    data.isPushLock = false;
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  void setTime(Timer timer) {
    if (scheduleActive) {
      DateTime now = DateTime.now();
      String nowTime = now.hour.toString().padLeft(2,'0')+now.minute.toString().padLeft(2,'0');

      debugPrint("onData");
      debugPrint(nowTime);
      debugPrint(scheduleStartTime);
      debugPrint(scheduleEndTime);

      if (int.parse(scheduleEndTime) <= int.parse(nowTime) || int.parse(scheduleStartTime) >= int.parse(nowTime)) {
        setState(() {
          wakeAlarmFlag = 1;
        });
      } else {
        setState(() {
          wakeAlarmFlag = -1;
        });
      }
    } else {
      setState(() {
        wakeAlarmFlag = 1;
      });
    }
  }

  Future<void> getVersion() async {
    version = await data.getVersion();
    setState(() {
      version = version;
    });
  }

  initSharedPreferences () async {
    //testing
//    int printCountData = await data.getPrinterCount();
//    bool success = await data.setPrinterCount(printerCount: printCountData-300);
    printCount = await data.getPrinterCount();
    if(printCount <= 10){
      serverApi.labelAlreadyFinish();
     setState(() {
       printerOn = 1;
     });
    } else {
      setState(() {
        printerOn = 0;
      });
    }
  }

  initFireStore () async {


    SharedPreferences prefs = await SharedPreferences.getInstance();
    comingSoonFlag = prefs.getInt('comingSoonFlag') ?? -1;

    Firestore.instance.collection(FirebaseConfig.BaseCollection)
        .document(FirebaseConfig.RemoteConfig)
        .collection(FirebaseConfig.KioskID).document("updateApp").snapshots().listen((snapshot) {

      snapshot.data.forEach((String key, dynamic data) {
        if (key == "mainAppUrl") {
          mainAppUrl = data;
        }

        if (key == "printAppUrl") {
          printerAppUrl = data;
        }
      });

    });

    Firestore.instance.collection(FirebaseConfig.BaseCollection).snapshots().listen((snapshot) {

      snapshot.documents.forEach((DocumentSnapshot documentSnapshot) {
        debugPrint("documentSnapshot.documentID:${documentSnapshot.documentID ?? ""}");

        if (documentSnapshot.documentID == FirebaseConfig.RemoteConfig) {
          documentSnapshot.data.forEach((String key, dynamic data) {
            debugPrint(key+":"+data);
            if (key == "repairImageUrl") {
              debugPrint(key+":"+data);
              setState(() {
                repairUrl = data.toString();
              });
            }
          });
        }

        if (documentSnapshot.documentID == FirebaseConfig.Schedule) {
          Schedule schedule = Schedule.fromSnapshot(documentSnapshot);
          scheduleActive = schedule.isActive;
          scheduleStartTime = convertTo24Hour(schedule.start);
          scheduleEndTime = convertTo24Hour(schedule.end);
          setTime(timer);
        }

      });

    });

  }

  String convertTo24Hour(String oriDateTime) {

    String hour = oriDateTime.split(":")[0];
    String min = oriDateTime.split(":")[1].split("am")[0].split("pm")[0];

    if (oriDateTime.contains("pm")){
      hour = (int.parse(oriDateTime.split(":")[0])+12).toString();
    }

    return hour+min;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig(context: context);
    return Scaffold(
      body: data.loadingUI(body:Container(
//        scrollDirection: Axis.vertical,
          child: Stack(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flex(
                    direction: Axis.horizontal,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
//                      Ink.image(image: AssetImage("assets/home_banner.png"), width: 1080, height: 500, child: InkWell(onTap: (){},child: null,),),
                          Image.asset("home_banner.png", width: Dimension.getFullWidth(context),),
//                      Image.asset("home_banner.png"),
                          appDownload ? Column(
                            children: <Widget>[
                              RaisedButton(
                                child: Container(
                                  child: Text("Update main app", style: TextStyle(fontWeight: FontWeight.bold),),
                                  margin: EdgeInsets.all(5),
                                ),
                                onPressed: () async {
                                  launch(mainAppUrl);
                                },
                              ),
                              Text("Version:$version", style: TextStyle(fontWeight: FontWeight.bold)

                              )
                            ],
                          ) : Container(),
                        ],
                      ),
//                Image.asset("home_banner.png"),
//                Image.asset("home_banner.png")
                    ],
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        height: 750 / Dimension.getRatio(context),
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 50.0 / Dimension.getRatio(context), bottom: 50.0/ Dimension.getRatio(context)),
                              child: Text(
                                "Please select your check-in method",
                                style: TextStyle(
                                  fontSize: 42.0 / Dimension.getRatio(context),
                                  fontWeight: FontWeight.w500,
                                  color: const Color(0xff333333),
                                ),),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 200.0/Dimension.getRatio(context), right: 180/Dimension.getRatio(context), top: 0/Dimension.getRatio(context)),
                              child: Text(
                                "Please scan a valid QR code",
                                maxLines: 1,
                                style: TextStyle(
                                    fontSize: 28/Dimension.getRatio(context),
                                    fontWeight: FontWeight.bold,
                                    color: isError ? const Color(0xFFD6322D) : Colors.transparent
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                GestureDetector(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 25.0/ Dimension.getRatio(context), bottom: 25/ Dimension.getRatio(context)),
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(30.0/ Dimension.getRatio(context)),
                                          side: BorderSide(
                                            color: Color(0xFF0066CC),
                                            width: 3/ Dimension.getRatio(context),
                                          )),
                                        color:  Colors.white,
                                        child: Container(
                                          width: 350/ Dimension.getRatio(context),
                                          height: 350/ Dimension.getRatio(context),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                            SizedBox(
                                            width: 250.0/ Dimension.getRatio(context),
                                            height: 250.0/ Dimension.getRatio(context),
                                            child: Image.asset(
                                                  "qr_effect.gif"
                                              )),
                                              Text("Online check-in",style: TextStyle(fontSize: 40/ Dimension.getRatio(context)))
                                            ],
                                          ),
                                        )
                                    ),
                                  ),
                                  onTap: () {

                                    SystemSound.play(SystemSoundType.click);

                                    if (_timer != null) {
                                      _timer.cancel();
                                    }

//                                    var guest = GuestModel(code: null, firstName: "", lastName: "Wong", email: "lihei02@gmail.com");
//                                    guest.setCompany("Testing limited");
//                                    guest.setPhoto("https://api.rxroche.com/uploads/images/guest/502_1563328294.png");

                                    Navigator.push(
                                      context,
//                            MaterialPageRoute(builder: (context) => ThankYou()),
//                              MaterialPageRoute(builder: (context) => CheckId(staff: Staff(name: "dickson",email: "dickson_yau@mtelnet.com") , guestList: [guest,guest,guest,guest,guest,guest,guest,guest,guest,guest,guest,guest,guest,guest,guest,],))
                                      MaterialPageRoute(builder: (context) => QrAccess(isEmployee: false)),
//                              MaterialPageRoute(builder: (context) => CheckinComplete(guests: [guest],)),
//                            MaterialPageRoute(builder: (context) => AddGuest(newGuest: guest,)),
//                            MaterialPageRoute(builder: (context) => PhotoTaking(popBackState: 0, guest: ModelGuest(code: "", firstName: "first", lastName: "last", email: ""))),
                                    );
                                  },
                                ),
                                GestureDetector(
                                  child: Container(
                                      margin: EdgeInsets.only(top: 25.0/ Dimension.getRatio(context), bottom: 25/ Dimension.getRatio(context)),
                                      child: CustomButton(resPath: "walkin_button.png")),
                                  onTap: () {
                                    if (_timer != null) {
                                      _timer.cancel();
                                    }
                                    SystemSound.play(SystemSoundType.click);
                                    Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(builder: (context) => Guest(isNew: false)),
                                    );
                                  },
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Spacer(),
                  Stack(
                    children: <Widget>[
                      TextField(
                        controller: _textController,
                        autofocus: true,
                        decoration: InputDecoration(
                        ),
                        style: TextStyle(
                          color: Colors.black,
                        ),
                        onChanged: (inputText) async {
                          if (inputText.length > 0) {
                            if (inputText != "") {

                              debugPrint("inputText:$inputText");
                             // sendInputRequest(inputText:inputText);
                            }
                          }
                        }
                      ),
                      data.bottomImage(imageName: "home_footer.png"),

                      Container(
                        alignment: Alignment.bottomRight,
                        child: GestureDetector(
                          child: (printerOn == 0) ? Container(
                            margin: EdgeInsets.only(top: 300 / Dimension.getRatio(context) ),
                            width: 300/ Dimension.getRatio(context),
                            height: 200/ Dimension.getRatio(context),
                            color: Colors.transparent,
                          ) : ((printerOn == 1) ?
                          Stack(
                            children: <Widget>[
                              Image.asset("reset_printer.png"),
                              Container(
                                margin: const EdgeInsets.only(top: 20, left: 10),
                                child: Text(printCount.toString()+"/300",style: TextStyle(fontSize: 40 / Dimension.getRatio(context))),
                              ),
                              GestureDetector(
                                child: Container(
                                    margin: const EdgeInsets.only(top: 140, left: 150),
                                    width: 280,
                                    height: 84,
                                    color: Colors.transparent
                                ),
                                onTap: () async {
                                  SystemSound.play(SystemSoundType.click);

                                  SharedPreferences prefs = await SharedPreferences.getInstance();
                                  await prefs.setInt('counter', 300);

                                  setState(() {
                                    printCount = 300;
                                    printerOn = 0;
                                  });
                                },
                              ),
                              GestureDetector(
                                child: Container(
                                    margin: const EdgeInsets.only(top: 0, left: 560),
                                    width: 50,
                                    height: 50,
                                    color: Colors.transparent
                                ),
                                onTap: () {
                                  SystemSound.play(SystemSoundType.click);
                                  setState(() {
                                    printerOn = 0;
                                  });
                                },
                              ),
                            ],
                          ) :
                          Stack(
                            children: <Widget>[
                              Image.asset("reset_printer_off.png"),
                              GestureDetector(
                                child: Container(
                                    margin: const EdgeInsets.only(top: 25.0, bottom: 25),
                                    child: CustomButton(resPath: "walkin_button.png")),
                                onTap: () {
                                  SystemSound.play(SystemSoundType.click);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => Guest(isNew: false)),
                                  );
                                },
                              ),
                            ],
                          )),
                          onTap: () {
                            SystemSound.play(SystemSoundType.click);

                            if (printerClick == 2) {

                              setState(() {
                                printerOn = 1;
                                printerClick = 0;
                              });

                            } else {

                              Timer.periodic(const Duration(seconds: 1), (Timer t) async {
                                if (t.tick == 1) {

                                  setState(() {
                                    printerClick = 0;
                                  });

                                  t.cancel();
                                }
                              });

                              setState(() {
                                printerClick = printerClick+1;
                              });
                            }

                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
             // data.progressHUD,
              (wakeAlarmFlag == 1) ? Container(
                  alignment: Alignment.topLeft,
                  color: Colors.black,
                  child:  CachedNetworkImage(imageUrl: (wakeAlarmFlag == 1) ? offImageUrl : "", placeholder: (context, url) => Image.asset("rest.jpg",height: Dimension.getFullHeight(context), fit: BoxFit.cover,),
                    height: Dimension.getFullHeight(context), fit: BoxFit.cover,)
              ) : Container(),
              (comingSoonFlag == 1) ? Container(
                  alignment: Alignment.topLeft,
                  color: Colors.black,
                  child: CachedNetworkImage(imageUrl: repairUrl, placeholder: (context, url) => Image.asset("coming_soon.jpg",height: Dimension.getFullHeight(context), fit: BoxFit.cover,),
                    height: Dimension.getFullHeight(context), fit: BoxFit.cover,)
              ) : Container(),
              Container(
                alignment: Alignment.bottomLeft,
                child: GestureDetector(
                  child: Container(
                    width: 300/ Dimension.getRatio(context),
                    height: 300/ Dimension.getRatio(context),
                    color: Colors.transparent,
                  ),
                  onDoubleTap: () async {
                    setState(() {
                      comingSoonFlag *= -1;
                    });

                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    await prefs.setInt('comingSoonFlag', comingSoonFlag);
                  },
                ),
              ),
              Container(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  child: Container(
                    width: 300/ Dimension.getRatio(context),
                    height: 300/ Dimension.getRatio(context),
                    color: Colors.transparent,
                  ),
                  onDoubleTap: () async {
                    setState(() {
                      appDownload = !appDownload;
                    });
                  },
                ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                child: GestureDetector(
                  child: Container(
                    width: 300/ Dimension.getRatio(context),
                    height: 300/ Dimension.getRatio(context),
                    color: Colors.transparent,
                  ),
                  onDoubleTap: () async {
                    setState(() {
                      printerOn = printerOn == 1 ? 0 : 1;
                    });
                  },
                ),
              ),
            ],
          )
      )),

    );
  }
}
