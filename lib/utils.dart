import 'package:flutter/material.dart';
import 'dart:io';
import 'package:http/io_client.dart';
import 'package:sentry/sentry.dart';
import 'dart:async';
import 'main.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

class CustomHttpClient {

  static IOClient http() {
    HttpClient client = new HttpClient();
    client.badCertificateCallback = (X509Certificate cert, String host, int port)=>true;

    IOClient ioClient = new IOClient(client);

    return ioClient;
  }

  String serverDomain = "https://roche-b2p-uat.mtel.ws";

  String bAuth = "Basic YWRtaW46YWRtaW4=";



  final SentryClient sentry = new SentryClient(environmentAttributes: Event(level: SeverityLevel.info, ), dsn: "https://f54c1e5eca8849aeb5ccb954350af3d8@sentry.io/2593066");


}

class Log {

  static FirebaseAnalytics analytics;
  static FirebaseAnalyticsObserver observer;

  static FirebaseAnalytics getAnalytics() {
    return analytics;
  }

  static FirebaseAnalyticsObserver getObserver() {
    return observer;
  }

  static Future<void> sendAnalyticsEvent(String key, String value, String action, String logLevel, ) async {
    await Log.analytics.logEvent(
      name: 'kiosk_event',
      parameters: <String, dynamic>{
        'logKey': key,
        'logValue': value,
        'logAction': action,
        'logLevel': "LOG",
        'bool': true,
      },
    );
  }

}

class Dimension {

  static double getFullWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  static double getFullHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  static double getRatio(BuildContext context) {
    return MediaQuery.of(context).devicePixelRatio;
  }

}

class FirebaseConfig {
  static String KioskID = "Kiosk01";
  static String BaseCollection = "DigitalKiosk-RHK";
  static String RemoteConfig = "remoteConfig";
  static String Schedule = "schedule";
  static String remoteKiosk = "DigitalKiosk-RHK/remoteConfig/"+KioskID;
  static String remoteKioskUpdateApp = "/DigitalKiosk-RHK/remoteConfig/Kiosk01/updateApp";
}

class Utils {
  static int timerCount = 90;
  static int timerStatus;
  static Timer _timer;

  static cancelTimer() {
    if (_timer != null) {
      _timer.cancel();
    }
  }

  static resetTimer() {
    timerCount = 90;
  }

  static startTimer({BuildContext context}) {
    cancelTimer();
    if (_timer == null || !_timer.isActive) {
      _timer = Timer.periodic(const Duration(seconds: 1), (Timer t) async {
        print("timerCount:${timerCount.toString()}");
        timerCount -= 1;
        if (timerCount <= 0) {
          t.cancel();

          if (context != null) {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MyApp()),
            );
          }

          timerStatus = -1;
        }
      });
    }

  }


}