import 'package:digital_reception/model/appointmentData.dart';
import 'package:digital_reception/model/singletonShareData.dart';
import 'package:digital_reception/view/SizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'view/header.dart';
import 'add_guest.dart';
import 'checkin_complete.dart';
import 'view/checkin_tile.dart';
import 'dart:convert' as convert;
import 'model/guestModel.dart';
import 'dart:convert';
import 'utils.dart';
import 'main.dart';
import 'dart:async';
//import 'package:progress_hud/progress_hud.dart';
import 'package:sentry/sentry.dart';
import 'package:flutter/services.dart';

class CheckId extends StatefulWidget {

  CheckId();
  //Add guest object arr;
//  List<GuestModel> guestList;
//  Staff staff;
//  String staffQr;

  @override
  _CheckIdState createState() => _CheckIdState();
//  State<StatefulWidget> createState() {
//   return _CheckIdState(guestList: guestList, staff: staff, staffQr: staffQr);
//  }

}

class _CheckIdState extends State<CheckId> {

//  final _scaffoldKey = GlobalKey<ScaffoldState>();
//  List<GuestModel> guestList;
  List<Widget> guestViews = new List();
 // List<Widget> columnOfRow = new List();
//  Staff staff;
  List<bool> flagList = new List();
//  ProgressHUD _progressHUD;
//  Timer _timer;
//  bool isDebug = false;
//  String staffQr;

//  _CheckIdState({ this.guestList, this.staff, this.staffQr });

  @override
  initState() {
    super.initState();
    data.isPushLock = false;
    data.showLoadingUI.listen((bool showLoading) {
      setState(() {
        data.isLoading = showLoading;
      });

    });
    rebuildUI();
    data.refreshCheckIdPage.listen((AppointmentData appointmentData) {
      setState(() {
        data.appointmentData = appointmentData;
        rebuildUI();
      });
//      rebuildUI();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    Utils.cancelTimer();
    super.dispose();
  }

//  bool flag = true;
//  _guestCard(GuestModel guest, int index) {
//    return CheckInTile(guest: guest);
//  }

  void rebuildUI(){
    guestViews = new List();
  //  columnOfRow = new List();
    flagList = new List();

    if (data.appointmentData.data != null) {
      for(int i=0;i<data.appointmentData.data.length;i++) {
        flagList.add(false);
        guestViews.add(Container(
          margin: EdgeInsets.only(left: 35.0, right: 35, top: 10, bottom: 10),
          child: CheckInTile(guest: data.appointmentData.data[i], callback:() {
            flagList[i] = !flagList[i];
            setState(() {
              flagList = flagList;
            });
          }),
        ));
      }
    }
    guestViews.add(
        Container(
          child: GestureDetector(
            child: Container(
              margin: const EdgeInsets.only(left: 35.0, right: 35, bottom: 25, top: 25),
              child: Image.asset("addguest_button.png"),
            ),
            onTap: () {
              SystemSound.play(SystemSoundType.click);
              Utils.cancelTimer();
              GuestModel modelGuest = GuestModel().emptyGuestModel();
              if(data.appointmentData != null){
                modelGuest.inviteeEmail = data.appointmentData.staffEmail;
              }
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddGuest(newGuest: modelGuest,appointmentData: data.appointmentData,addGuestType:AddGuestType.Employee)),
              );
            },
          ),
        )
    );

//    for(int i=0;i<guestViews.length/4;i++) {
//
//      for(int j=0;j<4;j++) {
//        List<Widget> dumList = new List();
//        if (4*i+j < guestViews.length) {
//          dumList.add(
//              RotatedBox(quarterTurns: 3, child: guestViews[4*i+j],)
//          );
//        }
//        columnOfRow.add(Container(height: 200, child: Row(children: dumList,),));
//      }
//
//    }


    Utils.startTimer(context: context);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: data.loadingUI(body:GestureDetector(
        onTap: () {
          Utils.resetTimer();
    },
    child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          physics: NeverScrollableScrollPhysics(),
          child: Stack(
            children: <Widget>[
              Column(
              mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Header(callback: () {
                    Utils.cancelTimer();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyApp()),
                    );
                  }, debugCallback: () {
                    setState(() {
//                      isDebug = !isDebug;
                    });
                  },),
                  Stack(
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(top: 50.0, bottom: 50, left: 0),
                              alignment: Alignment(0.0, 0.0),
                              child:  Container(),
                            ),
                            Container(
                              margin: const EdgeInsets.only(left: 100.0, right: 100),
                              child: Text(
                                "Please select the guest to check-in.",
                                style: TextStyle(
                                  fontSize: 42.0,
                                  fontWeight: FontWeight.w500,
                                  color: const Color(0xff333333),
                                ),),
                            ),

                                Container(
                                  height: 1000/ Dimension.getRatio(context),
                                  margin: const EdgeInsets.only(left: 20, right: 20),
                                  child:
                                  new GridView.builder(
                                    scrollDirection: Axis.vertical,
                                    physics: ScrollPhysics(),
                                    controller: ScrollController(),
                                    shrinkWrap: true,
                                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 4,
                                      mainAxisSpacing: 0,
                                      crossAxisSpacing: 0,
                                      childAspectRatio: (265/400),
                                    ),
                                    itemCount: (data.appointmentData == null ) ? 1 : (data.appointmentData.data == null) ? 1 : data.appointmentData.data.length + 1,
                                    itemBuilder: (BuildContext context, int index) {
                                      return guestViews[index];
                                    },
                                  ),
                                ),

                          ],
                        ),
                      ),

                    ],
                  ),
                  Flex(
                    direction: Axis.vertical,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              flex: 5,
                              child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: GestureDetector(
                                    child: Image.asset("print_label.png"),
                                    onTap: () async {
                                      SystemSound.play(SystemSoundType.click);
                                      bool alreadySelectGuest = false;
                                      for( var bool  in flagList){
                                        if(bool){
                                          alreadySelectGuest = true;
                                        }
                                      }
                                      if(alreadySelectGuest){
                                        data.sendRSVP(context: context,appointmentData: data.appointmentData,flagList: flagList);
                                      } else {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return CupertinoAlertDialog(
                                                title: new Text("Please at least select one guest"),
                                                actions: <Widget>[
                                                  CupertinoDialogAction(
                                                    child: Text("OK"),
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                  )
                                                ],
                                              );
                                            }
                                        );
                                      }



//                                      if (data.progressHUD != null) {
//                                        if (data.progressHUD.state != null) {
//                                          data.progressHUD.state.show();
//                                        }
//                                      }
//
//                                      List<Map<String, dynamic>> map = new List();
//                                      List<GuestModel> modelMap = new List();
//
//                                      for(int i=0;i<widget.appointmentData.data.length;i++) {
//                                        if (flagList[i]) {
//                                          map.add({"email": widget.appointmentData.data[i].guestEmail});
//                                         // modelMap.add(widget.appointmentData.data[i]);
//                                        }
//                                      }
////                                      debugPrint(map.toString());
//
//                                      String encodedJson = json.encode({
//                                        'staff': {
//                                          "email": staff.email,
//                                        },
//                                        "guests": map
//                                      });
//
//                                      CustomHttpClient().sentry.capture(event: Event(message: encodedJson, loggerName: "Guest walkInApprove request"));
//                                      Log.sendAnalyticsEvent("Guest walkInApprove", encodedJson, "api request","LOG");
//
//                                      var url = 'https://api.rxroche.com/api/kiosk/guests/walkInApprove';
//                                      var response = await CustomHttpClient.http().post(url, headers: {'authorization': 'Basic YWRtaW46YWRtaW4=', "Content-Type": "application/json"},
//                                          body: json.encode({
//                                            'staff': {
//                                              "email": staff.email,
//                                            },
//                                            "guests": map
//                                          })
//                                      );
//
//                                      CustomHttpClient().sentry.capture(event: Event(message: response.body
//                                          , loggerName: "Guest walkInApprove response"));
//                                      Log.sendAnalyticsEvent("Guest walkInApprove", response.body, "api response","LOG");
//
////                                      debugPrint("Response code: "+convert.jsonDecode(response.body)['return_code'].toString());
//
//                                      if (data.progressHUD != null) {
//                                        if (data.progressHUD.state != null) {
//                                          data.progressHUD.state.dismiss();
//                                        }
//                                      }
//
//                                      if (convert.jsonDecode(response.body)['return_code'] == 1) {
//                                        Utils.cancelTimer();
//                                        Navigator.push(
//                                          context,
//                                          MaterialPageRoute(builder: (context) => CheckInComplete(guests: modelMap,)),
//                                        );
//                                      } else {
//
//                                      }

                                    },
                                  )
                              )
                          ),
                        ],
                      ),
                      data.bottomImage(imageName:"footer_three.png" ),

                    ],
                  ),
                ],
              ),
            ],
          )
        )
    )));
  }

}
