import 'package:digital_reception/model/GuestInfo.dart';
import 'package:digital_reception/model/appointmentData.dart';
import 'package:digital_reception/model/singletonShareData.dart';
import 'package:digital_reception/view/SizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'view/header.dart';
import 'photo_taking.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:async';
import 'model/guestModel.dart';
import 'dart:math';
import 'dart:convert';
import 'checkin_complete.dart';
import 'thank_you.dart';
import 'checkid.dart';
import 'dart:convert' as convert;
import 'utils.dart';
import 'main.dart';
////import 'package:progress_hud/progress_hud.dart';
import 'package:sentry/sentry.dart';
import 'package:flutter/services.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

enum AddGuestType {
  Guest,
  Employee
}

class AddGuest extends StatefulWidget {

  List<GuestModel> guestList;
  GuestModel newGuest;
  String staffQr = "";
  AppointmentData appointmentData;
  AddGuestType addGuestType;
  AddGuest({ this.guestList, this.newGuest, this.staffQr,this.appointmentData,this.addGuestType});

  @override
  _AddGuestState createState() {
    return _AddGuestState(guestList: guestList, newGuest: newGuest, staffQr: staffQr );
  }
}

class _AddGuestState extends State<AddGuest> {

  _AddGuestState({ this.guestList, this.newGuest, this.staffQr });

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<GuestModel> guestList;
  List<String> inputText;
  List<StaffEmailList> inviteeList = [];
  List<StaffEmailList> inviteeFilterList = [];
  List<int> lengthLimit = [10,10,30,40,40];
  List<String> povList = ["Meeting", "Social events", "Workshop"];
  int fieldIdx = 0;
  double topIdx = 0;
  bool isAlpha = true;
  bool isCapital = false;
  bool isPopup = false;
  bool isTncOpened = false;
  bool isChecked = false;
  bool isMarketingChecked = true;
  String tncData = "";
  String inviteeUserEmail;
  String staffQr = "";
//  ProgressHUD _progressHUD;
  bool isPov = false;
  bool isInvitee = false;
  bool shouldEnableInvitee = true;
  GuestModel newGuest;
  List<bool> flagList = [false, false, false, false, false, false, false];
  Timer _timer;
  bool isDebug = false;

  ScrollController _scrollController;

  @override
  initState() {
    super.initState();
    data.showLoadingUI.listen((bool showLoading) {
      setState(() {
        data.isLoading = showLoading;
      });

    });
    setState((){
      inviteeList =  data.guestInfo == null ? List<StaffEmailList>() : data.guestInfo.data.staffEmailList;
      inviteeFilterList =  data.guestInfo == null ? List<StaffEmailList>() : data.guestInfo.data.staffEmailList;
      String inviteeName = "";

      if (newGuest.inviteeEmail != null) {
        if (newGuest.inviteeEmail != "") {
        inviteeName = newGuest.inviteeEmail;
      }}

      if (widget.appointmentData != null) {
          inviteeName = widget.appointmentData.staffEmail;
      }

      inputText = [newGuest.firstName, newGuest.lastName, newGuest.company, newGuest.purposeOfVisit, inviteeName, newGuest.email];
      if (inputText[5] == "") {
        setState(() {
          inputText[5] = povList[0];
        });
      }

    });

    getFileData("assets/tnc.txt").then((s) {
      setState(() {
        tncData = s;
      });
    });
    data.isPushLock = false;
    Utils.startTimer(context: context);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    Utils.cancelTimer();
    super.dispose();
  }

  _pageNavigate() async {

    if (!isChecked) {
      setState(() {
        flagList[6] = true;
      });
    }

    bool flag = true;
    for(int i=0; i< inputText.length; i++) {
      flag &= fieldValidate(i);
    }

    setState(() {
      flagList = flagList;
    });

    if (!flag) {
      if (fieldValidate(fieldIdx)) {
        if (fieldIdx >= 4) {

          setState(() {
            isPopup = false;
          });

          return;
        }
      }
      return;
    }

    if (!isChecked) {
      return;
    }

    GuestModel guest = GuestModel(
      code: newGuest.code,
      firstName: inputText[0],
      lastName: inputText[1],
      email: inputText[3],
    );

    guest.setAllowMarketing(isMarketingChecked);
    guest.setPurposeOfVisit(inputText[5]);
    guest.setCompany(inputText[2]);
    guest.inviteeEmail = inputText[4];
    if(inviteeUserEmail!=null){
      guest.inviteeEmail = inviteeUserEmail;
    }

    data.currentGuest = guest;

    AppointmentData appointmentData = await data.insertGuestInfo(context: context,guestModel: data.currentGuest,alreadyScanEmployeeQRCode: widget.appointmentData);
  }

  Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }

  bool fieldValidate(index) {

    debugPrint(inputText[index]);

    if (inputText[index] == "") {
      flagList[index] = true;
      return false;
    } else {
      flagList[index] = false;
    }

    if (index == 3) {
      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(inputText[index])) {
        flagList[index] = true;
        return false;
      }
    }

    return true;
  }

  Widget _overlayDropDown (BuildContext context) {
    double overlayMarginLeft = (newGuest.email != "") ? 900/Dimension.getRatio(context) : 1050/Dimension.getRatio(context);
    return !(isInvitee && inputText[4].length >= 2) ? Container(): Container(
      margin: EdgeInsets.only(left: 176.0/Dimension.getRatio(context), top: overlayMarginLeft/Dimension.getRatio(context), bottom: 176/Dimension.getRatio(context)),
      height: min(160.0, inviteeFilterList.length * 60.0)/Dimension.getRatio(context),
      width: 750/Dimension.getRatio(context),
      decoration: new BoxDecoration(
          color: const Color(0xFFFFFFFF), //new Color.fromRGBO(255, 0, 0, 0.0),
          borderRadius: new BorderRadius.only(bottomRight: const  Radius.circular(12.0), bottomLeft: const  Radius.circular(12.0),),
          border: new Border.all(color: const Color(0xFF979797))
      ),
      padding: EdgeInsets.only(top: 15.0/Dimension.getRatio(context), bottom: 15/Dimension.getRatio(context)),
      child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: inviteeFilterList.length,
          itemBuilder: (BuildContext ctxt, int index) {

            String inviteeName = inviteeFilterList[index].userFirstName+" "+inviteeFilterList[index].userLastName;
            return GestureDetector(
              child: Text(inviteeName,
                style: TextStyle(
                  fontSize: 24/Dimension.getRatio(context),
                  color: const Color(0xFF333333),
                ),),
              onTap: () {
                SystemSound.play(SystemSoundType.click);

//                if (isDebug) {
//                  _scaffoldKey.currentState.showSnackBar(SnackBar(
//                    content: Text("name: "+inviteeFilterList[index].name+", email:"+inviteeFilterList[index].email),
//                    duration: Duration(seconds: 2),
//                  ));
//                }

                setState(() {
                  inputText[4] = inviteeName;
                  inviteeUserEmail = inviteeFilterList[index].userEmail;
                  isInvitee = false;
                  isPopup = false;
                });
              },
            );
          }
      ),
    );
  }

  keyboardAdditionalDetection () {

    SystemSound.play(SystemSoundType.click);
    Utils.resetTimer();

    if (fieldIdx == 4) {
      List<StaffEmailList> filterList = List<StaffEmailList>();
      for(StaffEmailList staffEmailList in inviteeList){
        String searchText = inputText[fieldIdx].toLowerCase();
//        searchText = "Aida".toLowerCase();
        if(staffEmailList.userFirstName.toLowerCase().contains(searchText) || staffEmailList.userLastName.toLowerCase().contains(searchText)){
          filterList.add(staffEmailList);
        }
      }
//      List<StaffEmailList> filterList = inviteeList.where((staff) => staff.userFirstName.toLowerCase().contains(inputText[fieldIdx].toLowerCase())).toList();
      setState(() {
        inviteeFilterList =  filterList;
      });
      return;
    }

    int limitIdx = 20;

    switch (fieldIdx) {
      case  0:
        limitIdx = lengthLimit[0];
        break;

      case  1:
        limitIdx = lengthLimit[1];
        break;

      case  2:
        limitIdx = lengthLimit[2];
        break;

      case  4:
        limitIdx = lengthLimit[4];
        break;

      default:
        limitIdx = 40;
        break;

    }

    if (inputText[fieldIdx].length > limitIdx) {
      setState(() {
        inputText[fieldIdx]  = inputText[fieldIdx].substring(0,limitIdx);
      });
    }
  }

  void clickNextButton() {
    setState(() {
      isPopup = true;
      isInvitee = false;

      if(fieldIdx == 0 || fieldIdx == 1 || fieldIdx == 2){
        fieldIdx = fieldIdx+ 1;
      } else if(widget.addGuestType == AddGuestType.Guest) {
        if (fieldIdx == 3) {
          fieldIdx = 4;
          isInvitee = true;
        } else if (fieldIdx == 4) {
          isInvitee = true;
          fieldIdx = 4;
        }

//        else if (fieldIdx == 5) {
//          fieldIdx = 4;
//          isInvitee = true;
//        }
      }
      topIdx = topIdx == 4 ? 4 : topIdx+1;
    });
  }

  Widget _keyboard (BuildContext context) {
    if (isPopup) {
      String img = "keyboard_alpha.png";
      if (!isAlpha) {
        img = "keyboard_number.png";
      } else {
        if (!isCapital) {
          img = "keyboard_alpha.png";
        } else {
          img = "keyboard_alpha_cap.png";
        }
      }

      return Container(
        margin: EdgeInsets.only(top: (newGuest.email != "") ? 1040/Dimension.getRatio(context) : 1180/Dimension.getRatio(context)),
        alignment: Alignment.topCenter,
        child: Transform.scale(
          scale: 23/27,
          child: Stack(
            children: <Widget>[
              GestureDetector(
                child: Image.asset(img, width: 920,),
                onTap: () {

                },
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 8),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "Q" : "q" : "1";

                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 88),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "W" :"w" : "2";

                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 172),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "E" :"e" : "3";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 256),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "R" :"r" : "4";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 338),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "T" :"t" : "5";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 422),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "Y" :"y" : "6";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 506),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "U" :"u" : "7";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 590),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "I" :"i" : "8";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 674),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "O" :"o" : "9";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 758),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "P" :"p" : "0";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 840),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] = inputText[fieldIdx].substring(
                          0, inputText[fieldIdx].length - 1);
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 40),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "A" :"a" : "@";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 124),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "S" :"s" : "#";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 208),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "D" :"d" : "\$";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 290),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "F" :"f" : "&";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 372),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "G" :"g" : "*";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 454),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "H" :"h" : "(";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 536),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "J" :"j" : ")";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 618),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "K" :"k" : "\'";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 700),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "L" :"l" : "\"";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 780),
                width: 130,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () async {
                    //navigation
                    clickNextButton();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 8),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      if (isAlpha) {
                        isCapital = !isCapital;
                      } else {
                        inputText[fieldIdx] += "~";
                      }
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 88),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "Z" :"z" : "%";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 172),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "X" :"x" : "-";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 256),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "C" :"c" : "+";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 338),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "V" :"v" : "=";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 422),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "B" :"b" : "/";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 506),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "N" :"n" : ";";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 590),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? isCapital ? "M" :"m" : ":";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 674),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? "@" : ",";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 758),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += isAlpha ? "." : ".";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 840),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      if (isAlpha) {
                        inputText[fieldIdx] += ".com";
                      } else {
                        inputText[fieldIdx] += "\\";
                      }
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 228, left: 8),
                width: 150,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      isAlpha = !isAlpha;
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 228, left: 168),
                width: 500,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += " ";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 228, left: 680),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += "_";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 228, left: 760),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      inputText[fieldIdx] += "-";
                    });
                    keyboardAdditionalDetection();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 228, left: 840),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      isPopup = !isPopup;
                      isInvitee = false;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  _getTncWidget(flag) {
    if (flag) {
      return Container(
        padding: EdgeInsets.only(top: 100.0/Dimension.getRatio(context), left: 100/Dimension.getRatio(context), right: 100/Dimension.getRatio(context), bottom: 100/Dimension.getRatio(context)),
        alignment: Alignment.center,
        child: Stack(
          children: <Widget>[
            Card(
                elevation: 8,
                margin: EdgeInsets.only(top: 15.0/Dimension.getRatio(context), bottom: 15.0/Dimension.getRatio(context), left: 15/Dimension.getRatio(context), right: 15/Dimension.getRatio(context)),
                child: Container(
                    width: 1000,
                    height: 1000,
                    alignment: Alignment.center,
                    child: Scrollbar(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Container(
                            alignment: Alignment(0.0, 0.0),
                            padding: EdgeInsets.all(20/Dimension.getRatio(context)),
                            child: HtmlWidget(
                              tncData,
//                                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, height: 1.2),
                            ),
                          ),
                        ),
                    )
                ),
            ),
            GestureDetector(
              child: Container(
                alignment: Alignment.topLeft,
                child: Image.asset("cross_button.png"), width: 44/1080*Dimension.getFullWidth(context),),
              onTap: () {
                SystemSound.play(SystemSoundType.click);
                setState(() {
                  isTncOpened = false;
                });
              },
            ),
          ],
        ),
      );
    } else {
      return new Container();
    }
  }

  @override
  Widget build(BuildContext context) {

    double keyboardTopPadding = 0;
    if(topIdx == 0 || topIdx == 3 || topIdx == 4){
      keyboardTopPadding = (1300/ Dimension.getRatio(context)).toDouble();
    } else if(topIdx == 1 ){
      keyboardTopPadding = (1160/ Dimension.getRatio(context)).toDouble();
    } else if(topIdx == 2){
      keyboardTopPadding = (1000/ Dimension.getRatio(context)).toDouble();
    }

    return Scaffold(
        key: _scaffoldKey,
        body: GestureDetector(
                onTap: () {
                  Utils.resetTimer();
                  isPopup = false;
            },
            child: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
                child: Container(
          child: Stack(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  (widget.appointmentData == null) ? Header(callback: () {
                    Utils.cancelTimer();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyApp()),
                    );
                  },debugCallback: () {
                    setState(() {
                      isDebug = !isDebug;
                    });
                  }) : Header(isBack: widget.appointmentData.staffEmail, callback: (){
                    Utils.cancelTimer();
                    Navigator.pop(context, false);
                  },debugCallback: () {
                    setState(() {
                      isDebug = !isDebug;
                    });
                  }),
                 SingleChildScrollView(child: Container(
                    height: 1150 / Dimension.getRatio(context),
                    child: Card(
                        elevation: 2,
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 50.0 / Dimension.getRatio(context), bottom: 15 / Dimension.getRatio(context)),
                                child: Text(
                                  "Registration",
                                  style: TextStyle(
                                    fontSize: 42.0 / Dimension.getRatio(context),
                                    fontWeight: FontWeight.w500,
                                    color: const Color(0xff333333),
                                  ),),
                              ),
                              GestureDetector(
                                child: Container(
                                    child: Card(
                                        elevation: 0,
                                        child: Container(
                                          margin: EdgeInsets.only(top: 5/ Dimension.getRatio(context), bottom: 5/ Dimension.getRatio(context)),
                                          child: Column(
                                            children: <Widget>[
                                              Stack(
                                                children: <Widget>[
                                                  Container(
                                                    alignment: Alignment(0.0, 0.0),
                                                    child: (fieldIdx == 0) ? Image.asset("first_name_on.png", width: Dimension.getFullWidth(context) * 0.7129,) : Image.asset("first_name_off.png", width: Dimension.getFullWidth(context) * 0.7129),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 180.0 / Dimension.getRatio(context), right: 180 / Dimension.getRatio(context), top: 55 / Dimension.getRatio(context)),
                                                    alignment: Alignment.centerLeft,
                                                    child: Text(
                                                      inputText[0],
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                        fontSize: 30 / Dimension.getRatio(context),
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 840.0 / Dimension.getRatio(context), right: 180 / Dimension.getRatio(context), top: 95 / Dimension.getRatio(context)),
                                                    alignment: Alignment.centerLeft,
                                                    child: Text(
                                                      inputText[0].length.toString()+"/"+lengthLimit[0].toString(),
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 14 / Dimension.getRatio(context),
                                                          color: const Color(0xFFA7A7A7)
                                                      ),
                                                      textAlign: TextAlign.right,
                                                    ),
                                                  ),
                                                  (flagList[0]) ? Container(
                                                    margin: EdgeInsets.only(left: 180.0 / Dimension.getRatio(context), right: 180 / Dimension.getRatio(context), top: 95 / Dimension.getRatio(context)),
                                                    child: Text(
                                                      "Please enter your first name",
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 14 / Dimension.getRatio(context),
                                                          fontWeight: FontWeight.bold,
                                                          color: const Color(0xFFD6322D)
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ) : Container()
                                                ],
                                              ),
                                            ],
                                          ),
                                        )
                                    )
                                ),
                                onTap: () {
                                  SystemSound.play(SystemSoundType.click);
                                  setState(() {
                                    isPopup = true;
                                    isInvitee = false;
                                    fieldIdx = 0;
                                    topIdx = 0;
                                  });
                                  Utils.resetTimer();
                                },
                              ),
                              GestureDetector(
                                child: Container(
                                    margin: const EdgeInsets.only(left: 0.0, right: 0),
                                    alignment: Alignment(0.0, 0.0),
                                    child: Card(
                                        elevation: 0,
                                        child: Container(
                                          margin: EdgeInsets.only(top: 5/ Dimension.getRatio(context), bottom: 5/ Dimension.getRatio(context)),
                                          child: Column(
                                            children: <Widget>[
                                              Stack(
                                                children: <Widget>[
                                                  Container(
                                                    alignment: Alignment(0.0, 0.0),
                                                    child: (fieldIdx == 1) ? Image.asset("last_name_on.png", width: Dimension.getFullWidth(context) * 0.7129) : Image.asset("last_name_off.png", width: Dimension.getFullWidth(context) * 0.7129),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 180.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 55/ Dimension.getRatio(context)),
                                                    alignment: Alignment.centerLeft,
                                                    child: Text(
                                                      inputText[1],
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                        fontSize: 30/ Dimension.getRatio(context),
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 840.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 95/ Dimension.getRatio(context)),
                                                    alignment: Alignment.centerLeft,
                                                    child: Text(
                                                      inputText[1].length.toString()+"/"+lengthLimit[1].toString(),
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 14/ Dimension.getRatio(context),
                                                          color: const Color(0xFFA7A7A7)
                                                      ),
                                                      textAlign: TextAlign.right,
                                                    ),
                                                  ),
                                                  (flagList[1]) ? Container(
                                                    margin: EdgeInsets.only(left: 180.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 95/ Dimension.getRatio(context)),
                                                    child: Text(
                                                      "Please enter your last name",
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 14/ Dimension.getRatio(context),
                                                          fontWeight: FontWeight.bold,
                                                          color: const Color(0xFFD6322D)
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ) : Container()
                                                ],
                                              ),
                                            ],
                                          ),
                                        )
                                    )
                                ),
                                onTap: () {
                                  SystemSound.play(SystemSoundType.click);
                                  setState(() {
                                    isPopup = true;
                                    isInvitee = false;
                                    fieldIdx = 1;
                                    topIdx = 1;
                                  });
                                  Utils.resetTimer();
                                },
                              ),
                              GestureDetector(
                                child: Container(
                                    margin: const EdgeInsets.only(left: 0.0, right: 0),
                                    alignment: Alignment(0.0, 0.0),
                                    child: Card(
                                        elevation: 0,
                                        child: Container(
                                          margin: EdgeInsets.only(top: 5/ Dimension.getRatio(context), bottom: 5/ Dimension.getRatio(context)),
                                          child: Column(
                                            children: <Widget>[
                                              Stack(
                                                children: <Widget>[
                                                  Container(
                                                    alignment: Alignment(0.0, 0.0),
                                                    child: (fieldIdx == 2) ? Image.asset("company_on.png", width: Dimension.getFullWidth(context) * 0.7129) : Image.asset("company_off.png", width: Dimension.getFullWidth(context) * 0.7129),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 180.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 50/ Dimension.getRatio(context)),
                                                    alignment: Alignment.centerLeft,
                                                    child: Text(
                                                      inputText[2],
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                        fontSize: 30/ Dimension.getRatio(context),
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 840.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 95/ Dimension.getRatio(context)),
                                                    alignment: Alignment.centerLeft,
                                                    child: Text(
                                                      inputText[2].length.toString()+"/"+lengthLimit[2].toString(),
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 14/ Dimension.getRatio(context),
                                                          color: const Color(0xFFA7A7A7)
                                                      ),
                                                      textAlign: TextAlign.right,
                                                    ),
                                                  ),
                                                  (flagList[2]) ? Container(
                                                    margin: EdgeInsets.only(left: 180.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 95/ Dimension.getRatio(context)),
                                                    child: Text(
                                                      "Please enter your company",
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 14/ Dimension.getRatio(context),
                                                          fontWeight: FontWeight.bold,
                                                          color: const Color(0xFFD6322D)
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ) : Container()
                                                ],
                                              ),
                                            ],
                                          ),
                                        )
                                    )
                                ),
                                onTap: () {
                                  SystemSound.play(SystemSoundType.click);
                                  setState(() {
                                    isPopup = true;
                                    isInvitee = false;
                                    fieldIdx = 2;
                                    topIdx = 2;
                                  });
                                  Utils.resetTimer();
                                },
                              ),
                              (newGuest.email != "") ? Container() :GestureDetector(
                                child: Container(
                                    margin: const EdgeInsets.only(left: 0.0, right: 0),
                                    alignment: Alignment(0.0, 0.0),
                                    child: Card(
                                        elevation: 0,
                                        child: Container(
                                          margin: EdgeInsets.only(top: 5/ Dimension.getRatio(context), bottom: 5/ Dimension.getRatio(context)),
                                          child: Column(
                                            children: <Widget>[
                                              Stack(
                                                children: <Widget>[
                                                  Container(
                                                    alignment: Alignment(0.0, 0.0),
                                                    child: (fieldIdx == 3) ? Image.asset("email_on.png", width: Dimension.getFullWidth(context) * 0.7129) : Image.asset("email_off.png", width: Dimension.getFullWidth(context) * 0.7129),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 180.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 50/ Dimension.getRatio(context)),
                                                    alignment: Alignment.centerLeft,
                                                    child: Text(
                                                      inputText[3],
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                        fontSize: 30/ Dimension.getRatio(context),
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 840.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 86/ Dimension.getRatio(context)),
                                                    alignment: Alignment.centerLeft,
                                                    child: Text(
                                                      inputText[3].length.toString()+"/"+lengthLimit[3].toString(),
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 14/ Dimension.getRatio(context),
                                                          color: const Color(0xFFA7A7A7)
                                                      ),
                                                      textAlign: TextAlign.right,
                                                    ),
                                                  ),
                                                  (flagList[3]) ? Container(
                                                    margin: EdgeInsets.only(left: 180.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 86/ Dimension.getRatio(context)),
                                                    child: Text(
                                                      "Please enter a valid email",
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 14/ Dimension.getRatio(context),
                                                          fontWeight: FontWeight.bold,
                                                          color: const Color(0xFFD6322D)
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ) : Container()
                                                ],
                                              ),
                                            ],
                                          ),
                                        )
                                    )
                                ),
                                onTap: () {
                                  SystemSound.play(SystemSoundType.click);
                                  setState(() {
                                    isPopup = true;
                                    isInvitee = false;
                                    fieldIdx = 3;
                                    topIdx = 3;
                                  });
                                  Utils.resetTimer();
                                },
                              ),
                              GestureDetector(
                                child: Container(
                                    margin: const EdgeInsets.only(left: 0.0, right: 0),
                                    alignment: Alignment(0.0, 0.0),
                                    child: Card(
                                        elevation: 0,
                                        child: Container(
                                          margin: EdgeInsets.only(top: 5/ Dimension.getRatio(context), bottom: 5/ Dimension.getRatio(context)),
                                          child: Column(
                                            children: <Widget>[
                                              Stack(
                                                children: <Widget>[
                                                  Container(
                                                    alignment: Alignment(0.0, 0.0),
                                                    child: (fieldIdx == 4) ? Image.asset("invitee_on.png", width: Dimension.getFullWidth(context) * 0.7129) : Image.asset("invitee_off.png", width: Dimension.getFullWidth(context) * 0.7129),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 180.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 50/ Dimension.getRatio(context)),
                                                    alignment: Alignment.centerLeft,
                                                    child: Text(
                                                      inputText[4],
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                        fontSize: 30/ Dimension.getRatio(context),
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                  (flagList[4]) ? Container(
                                                    margin: EdgeInsets.only(left: 180.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 93/ Dimension.getRatio(context)),
                                                    child: Text(
                                                      "Please enter a valid invitee name",
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 14/ Dimension.getRatio(context),
                                                          fontWeight: FontWeight.bold,
                                                          color: const Color(0xFFD6322D)
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ) : Container()
                                                ],
                                              ),
                                            ],
                                          ),
                                        )
                                    )
                                ),
                                onTap: () {
                                  if (widget.addGuestType == AddGuestType.Guest) {
                                    SystemSound.play(SystemSoundType.click);
                                    setState(() {
                                      isPopup = true;
                                      isInvitee = true;
                                      fieldIdx = 4;
                                      topIdx = 4;
                                    });
                                  }
                                  Utils.resetTimer();
                                },
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10/ Dimension.getRatio(context), left: 150.0/ Dimension.getRatio(context), right: 80/ Dimension.getRatio(context)),
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: povList.map((s) =>
                                        Container(
                                          child: Transform.scale(
                                            scale: 1.0,
                                            child: Row(
                                              children: <Widget>[
                                                Radio(value: s, groupValue: inputText[5], onChanged: (s) { setState(() { inputText[5] = s; }); },),
                                                Text(s, style: TextStyle(fontSize: 30/Dimension.getRatio(context)),)
                                              ],
                                            ),)
                                        )
                                    ).toList(),
                                  ),
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(left: 140.0/ Dimension.getRatio(context), right: 140/ Dimension.getRatio(context)),
                                  alignment: Alignment.centerLeft,
                                  child: Stack(
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(top: 20/ Dimension.getRatio(context), left: 50/ Dimension.getRatio(context)),
                                        child: Text(
                                          "I agree to receive the email from Roche about the events and activities update",
                                          style: TextStyle(fontSize: 26/ Dimension.getRatio(context)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 13/ Dimension.getRatio(context), left: 0),
                                        padding: EdgeInsets.all(0),
                                        child: Checkbox(
                                            value: isMarketingChecked,
                                            onChanged: (bool v) {
                                              setState(() {
                                                isMarketingChecked = v;
                                              });
                                            }),
                                      ),
                                    ],
                                  ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(left: 140.0/ Dimension.getRatio(context), right: 140/ Dimension.getRatio(context)),
                                  alignment: Alignment.centerLeft,
                                  child: Stack(
                                    children: <Widget>[
                                      Container(
                                          margin: EdgeInsets.only(top: 20/ Dimension.getRatio(context), left: 50/ Dimension.getRatio(context)),
                                          child: RichText(
                                            text: TextSpan(
                                              style: TextStyle(fontSize: 26/ Dimension.getRatio(context), color: Colors.black),
                                              children: <TextSpan>[
                                                TextSpan(text: "I agree to be bound by, the terms and conditions including the Privacy Policy (together, the “",),
                                                TextSpan(
                                                  text: "Terms",
                                                  style: TextStyle(fontWeight: FontWeight.bold, color: const Color(0xff0066CC)),
                                                ),
                                                TextSpan(text: "”). "),
                                              ],
                                            ),
                                          )
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 50/ Dimension.getRatio(context), left: 390/ Dimension.getRatio(context)),
                                        width: 70/ Dimension.getRatio(context),
                                        height: 30/ Dimension.getRatio(context),
                                        child: GestureDetector(
                                          child: Container(color: Colors.transparent,),
                                          onTap: () {
                                            SystemSound.play(SystemSoundType.click);
                                            setState(() {
                                              isTncOpened = true;
                                            });
                                          },
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 13/ Dimension.getRatio(context), left: 0),
                                        child: Checkbox(
                                            value: isChecked,
                                            onChanged: (bool v) {
                                              setState(() {
                                                isChecked = v;
                                                if (isChecked) {
                                                  flagList[6] = false;
                                                }
                                              });
                                            }),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 80/ Dimension.getRatio(context), left: 50/ Dimension.getRatio(context)),
                                        child: Text(
                                          "Please accept the terms and conditions",
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 14/ Dimension.getRatio(context),
                                              fontWeight: FontWeight.bold,
                                              color: (flagList[6]) ? const Color(0xFFD6322D) : Colors.transparent
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      )
                                    ],
                                  )
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 20/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context)),
                                  alignment: Alignment.bottomRight,
                                  child: GestureDetector(
                                    child: Image.asset("next_button.png", width: 195/1080*Dimension.getFullWidth(context),),
                                    onTap: () async {
                                      SystemSound.play(SystemSoundType.click);
                                      _pageNavigate();

                                    },
                                  )
                              )
                            ],
                          ),
                        )
                    ),
                  )),
//                  Spacer(),
                  Flex(
                    direction: Axis.vertical,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Expanded(
                              flex: 5,
                              child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: GestureDetector(
                                    child: Image.asset("regist_on.png"),
                                    onTap: () {

                                    },
                                  )
                              )
                          ),
                          Expanded(
                              flex: 5,
                              child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: GestureDetector(
                                    child: Image.asset("photo_off.png"),
                                    onTap: () {

                                    },
                                  )
                              )
                          ),
                        ],
                      ),
                      data.bottomImage(imageName: "footer_three.png"),

                    ],
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 50.0/ Dimension.getRatio(context), bottom: 50/ Dimension.getRatio(context), left: 0),
                alignment: Alignment.topCenter,
                child: isDebug? Text(
                  Utils.timerCount.toString(),
                  style: TextStyle(
                    fontSize: 15.0/ Dimension.getRatio(context),
                    fontWeight: FontWeight.w500,
                    color: const Color(0xff333333),
                  ),): Container(),
              ),
                Positioned(
                  left: 0,
                  right: 0,
                  bottom: keyboardTopPadding,
                  child:_keyboard(context)),
              _overlayDropDown(context),
              isTncOpened ? Container(
                width: 2100/ Dimension.getRatio(context),
                height: 1500/ Dimension.getRatio(context),
                color: const Color.fromARGB(0xaa, 91, 91, 91),
              ): Container(),
              _getTncWidget(isTncOpened),
           //   data.progressHUD
            ],
          )
        ))
    ));
  }

}
