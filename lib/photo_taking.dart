import 'package:flutter/material.dart';
import 'checkin_complete.dart';
import 'add_guest.dart';
import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'guest.dart';
import 'checkid.dart';
import 'thank_you.dart';
import 'dart:ui';
import 'package:flutter/rendering.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'model/guestModel.dart';
import 'package:image/image.dart' as Im;
import 'qraccess.dart';
//import 'package:progress_hud/progress_hud.dart';
import 'dart:convert' as convert;
import 'utils.dart';

class PhotoTaking extends StatefulWidget {

  int popBackState;
  //make a Guest object
  GuestModel guest;
  List<GuestModel> additionalGuest;
  Staff staff;

  PhotoTaking({ this.popBackState, this.guest, this.additionalGuest, this.staff });

  @override
  _PhotoTakingState createState() => _PhotoTakingState(popBackState: popBackState, guest: guest, additionalGuest: additionalGuest, staff: staff);

}

class _PhotoTakingState extends State<PhotoTaking> {

  _PhotoTakingState({ this.popBackState, this.guest, this.additionalGuest, this.staff });

  final GlobalKey<State<StatefulWidget>> scaffoldKey = GlobalKey();
  List<CameraDescription> cameras;
  CameraController controller;
  bool isReady = false;
  bool showCamera = true;
  File imagePath;
  String imageUri = "";
  Staff staff;

  int popBackState;
  bool isChecked = false;
  bool isTncOpened = false;
  String tncData = "";

  Uint8List bytes;
  Uint8List lastBytes;

  List<GuestModel> additionalGuest;
  GuestModel guest;
  int countdown = 3;
  String cameraHint = "Touch here to take photo";

  GlobalKey key = GlobalKey();
//  ProgressHUD _progressHUD;

  Timer _timer;

  @override
  initState() {
    super.initState();
    setupCameras();
//    _progressHUD = new ProgressHUD(
//      backgroundColor: Colors.transparent,
//      color: Colors.white,
//      containerColor: const Color(0xff9a9a9a),
//      borderRadius: 5.0,
//      text: 'Checking',
//      loading: false,
//    );


    Utils.resetTimer();
    Utils.startTimer(context: context);
  }

  startReturnTimer() async {

  }

  @override
  void dispose() {
    _timer.cancel();
    controller.dispose();
    super.dispose();
  }

  Future<void> setupCameras() async {
    try {
      cameras = await availableCameras();
      if (cameras != null && cameras.length > 0) {
        controller = new CameraController(cameras[0], ResolutionPreset.high);
      }
      await controller.initialize();
    } on CameraException catch (_) {
      setState(() {
        isReady = false;
      });
    }

    setState(() {
      isReady = true;
    });
  }

  _byteImageView() {
    if (countdown >= 0) {
      return RepaintBoundary(
          key: scaffoldKey,
          child:Stack(
          children: <Widget>[
            Container(
              child: isReady? Center(
                child: AspectRatio(
                    aspectRatio: 97/77,
                    child: new CameraPreview(controller)),
              ) : Container(),
            ),
            Center(child: Image.asset("mask_circle.png"),),
          ],
        ));
    } else {

      Widget lastFrame = Image.file(File(imageUri), width: 800, height: 860, fit: BoxFit.fill,);

      return Stack(
        children: <Widget>[
          lastFrame,
        Center(child: Image.asset("mask_circle.png"),),
          Container(
              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                child: Image.asset("confirm_button.png"),
                onTap: () async {

//                  _progressHUD.state.show();

                  String base = base64Encode(File(imageUri).readAsBytesSync());
                  Im.Image image = Im.copyResizeCropSquare(Im.decodeImage(File(imageUri).readAsBytesSync()),200);
                  base = base64Encode(Im.encodePng(image));
                  guest.setPhoto(base);

                  if (additionalGuest == null) {

                    List<Map<String, dynamic>> map = new List();
                    map.add(guest.toJson());

                    var url = 'https://api.rxroche.com/api/kiosk/guests/checkin';
                    var response = await CustomHttpClient.http().post(url, headers: {'authorization': 'Basic YWRtaW46YWRtaW4=', "Content-Type": "application/json" },
                        body: json.encode({'event': { "code": guest.code }, "guests": map }));

//                    _progressHUD.state.dismiss();

                    if (convert.jsonDecode(response.body)['return_code'] == 1) {

                      if (guest.code == "") {
                        Utils.cancelTimer();
                        Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => ThankYou())
                        );
                      } else {
                        Utils.cancelTimer();
                        Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => CheckInComplete(guests: [guest],))
                        );
                      }

                    }

                  } else {
//                    _progressHUD.state.dismiss();
                    additionalGuest.add(guest);
                    Utils.cancelTimer();
//                    Navigator.push(
//                        context,
//                        MaterialPageRoute(builder: (context) => CheckId(staff: staff, guestList: additionalGuest,))
//                    );
                  }
                },
              )
          )
        ],
      );
    }
  }

  _touchArea() {
    if (countdown <= 0) {
      return Container();
    } else {
      return
        Container(
          margin: const EdgeInsets.only(top: 110, bottom: 0),
          alignment: Alignment.topCenter,
          child: Stack(
            children: <Widget>[
              GestureDetector(
                child: Container(
                    margin: const EdgeInsets.only(top: 120),
                    width: 200,
                    height: 200,
                    alignment: Alignment.center,
                    child: Stack(
                      alignment: AlignmentDirectional.center,
                      children: <Widget>[
                        Image.asset("camera_touch.png"),
                        Container(
                          width: 200,
                          height: 400,
                          alignment: Alignment.center,
                          child: Text(
                            cameraHint,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 30.0,
                              fontWeight: FontWeight.w500,
                              color: const Color(0xff0066CC),
                            ),
                          ),
                        )
                      ],
                    )
                ),
                onTap: () async {
                  if (countdown == 3) {
                    await startTimer();
                  }
                },
              )
            ],
          ),
        );
    }
  }

  _popBackWidget() {
    switch (popBackState) {
      case 0:
        return GestureDetector(
          child: Image.asset("email_button_off.png"),
          onTap: () {
            Utils.cancelTimer();
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Guest(isNew: false))
            );
          },
        );
        break;

      case 1:
        return GestureDetector(
          child: Image.asset("regist_off.png"),
          onTap: () {
            Utils.cancelTimer();
//            Navigator.push(
//                context,
//                MaterialPageRoute(builder: (context) => AddGuest(newGuest: guest,))
//            );
          },
        );
        break;

      case 2:
        return GestureDetector(
          child: Image.asset("regist_off.png"),
          onTap: () {
            Utils.cancelTimer();
//            Navigator.push(
//                context,
//                MaterialPageRoute(builder: (context) => AddGuest(newGuest: guest,))
//            );
          },
        );

      case 3:
        return GestureDetector(
          child: Image.asset("scan_qr_off.png"),
          onTap: () {
            Utils.cancelTimer();
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => QrAccess(isEmployee: false))
            );
          },
        );
        break;
    }

  }

  startTimer() async {

    setState(() {
      cameraHint = (countdown).toString();
    });

    Timer _timer = Timer.periodic(const Duration(seconds: 1), (Timer t) async {

      if (countdown == 1) {
        final Directory appDirectory = await getApplicationDocumentsDirectory();
        final String pictureDirectory = '${appDirectory.path}/Pictures';
        await Directory(pictureDirectory).create(recursive: true);
        final String currentTime = DateTime.now().millisecondsSinceEpoch.toString();
        final String filePath = '$pictureDirectory/${currentTime}.jpg';

        await controller.takePicture(filePath);

        File imagefile = new File(filePath);

        setState(() {
          imagePath = imagefile;
          imageUri = filePath;
        });
      }


      if (countdown == 0) {
        t.cancel();

//        final Directory appDirectory = await getApplicationDocumentsDirectory();
//        final String pictureDirectory = '${appDirectory.path}/Pictures';
//        await Directory(pictureDirectory).create(recursive: true);
//        final String currentTime = DateTime.now().millisecondsSinceEpoch.toString();
//        final String filePath = '$pictureDirectory/${currentTime}.jpg';
//
//        await controller.takePicture(filePath);
//
//        File imagefile = new File(filePath);
//
//        setState(() {
//          imagePath = imagefile;
//          imageUri = filePath;
//        });
      }

      setState(() {
        countdown -= 1;
        cameraHint = (countdown).toString();
      });

    });

  }



  @override
  Widget build(BuildContext context) {
    // TODO: implegment build
    return Scaffold(
        body: GestureDetector(
            onTap: () {

            },
            child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
                child: Stack(
                  children: <Widget>[
                    Column(
                      // Column is also layout widget. It takes a list of children and
                      // arranges them vertically. By default, it sizes itself to fit its
                      // children horizontally, and tries to be as tall as its parent.
                      //
                      // Invoke "debug painting" (press "p" in the console, choose the
                      // "Toggle Debug Paint" action from the Flutter Inspector in Android
                      // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
                      // to see the wireframe for each widget.
                      //
                      // Column has various properties to control how it sizes itself and
                      // how it positions its children. Here we use mainAxisAlignment to
                      // center the children vertically; the main axis here is the vertical
                      // axis because Columns are vertical (the cross axis would be
                      // horizontal).
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          color: Colors.white,
                          width: 1400,
                          child: Column(
                            children: <Widget>[
                              Container(
                                alignment: Alignment(0.0, 0.0),
                                child: Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: <Widget>[
                                    Image.asset("photo_reminder.png", width: 457,),
                                    Container(
                                      alignment: Alignment(0.0, 0.0),
                                      margin: const EdgeInsets.only(top: 85, bottom: 0),
                                      child: Text(
                                        guest.firstName + " " + guest.lastName,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: 55.0,
                                          fontWeight: FontWeight.bold,
                                          color: const Color(0xff0066CC),
                                        ),),
                                    ),
                                  ],
                                ),
                              ),
                              Card(
                                  elevation: 0,
                                  child: Container(
                                      margin: const EdgeInsets.only(top: 30),
                                      alignment: Alignment(0.0, 0.0),
                                      child: Stack(
                                        alignment: Alignment(0.0, 0.0),
                                        children: <Widget>[
                                          Container(
                                              alignment: Alignment(0.0, 0.0),
                                              width: 860,
                                              height: 770,
                                              child: Stack(
                                                children: <Widget>[
                                                  Stack(
                                                    children: <Widget>[
//                                                  (imageUri != "") ? Container() : Center(child: Image.asset("dotted_border.png"),),
                                                      _byteImageView(),
                                                      _touchArea(),
                                                    ],
                                                  )
                                                ],
                                              )
                                          ),
                                          Container(
                                            margin: const EdgeInsets.only(left: 90),
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              width: 150,
                                              height: 860,
                                              color: Colors.white,
                                            ),
                                          ),
                                          Container(
                                            margin: const EdgeInsets.only(left: 750),
                                            alignment: Alignment(0.0, 0.0),
                                            child: Container(
                                              width: 200,
                                              height: 860,
                                              color: Colors.white,
                                            ),
                                          ),
                                          (countdown <= 0) ? Container(
                                            alignment: Alignment.bottomLeft,
                                            child: GestureDetector(
                                              child: Image.asset("retake_button.png"),
                                              onTap: () {
                                                setState(() {
                                                  countdown = 3;
                                                  imageUri = "";
                                                  cameraHint = "Touch here to take photo";
                                                });
                                              },
                                            ),
                                          ) : Container()
                                        ],
                                      )
                                  )
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 10.0, bottom: 80),
                                child: Text(
                                  "*Disclaimer: We are not collecting any biometric data",
                                  style: TextStyle(
                                    fontSize: 26.0,
                                    fontWeight: FontWeight.w500,
                                    color: const Color(0xff333333),
                                  ),),
                              ),
                            ],
                          ),
                        ),
                        Flex(
                          direction: Axis.vertical,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Expanded(
                                    flex: 5,
                                    child: Align(
                                        alignment: Alignment.bottomCenter,
                                        child: _popBackWidget()
                                    )
                                ),
                                Expanded(
                                    flex: 5,
                                    child: Align(
                                        alignment: Alignment.bottomCenter,
                                        child: GestureDetector(
                                          child: Image.asset("photo_on.png"),
                                          onTap: () {

                                          },
                                        )
                                    )
                                ),
                              ],
                            ),
                            Image.asset("footer_three.png")
                          ],
                        ),
                      ],
                    ),
                    (countdown == 10) ? Container(
                        width: 1500,
                        height: 2100,
                        color: Colors.white,
                        child: Container(
                          margin: const EdgeInsets.only(top: 40),
                          alignment: Alignment.topCenter,
                          child: Text("Please keep your smiling face to the camera", style: TextStyle(fontSize: 42, color: const Color(0xFF0066CC)),),
                        ),
                    ) : Container(),
//                    _progressHUD
                  ],
                )
            )
        )
    );
  }

}
