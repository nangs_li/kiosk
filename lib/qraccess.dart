import 'package:digital_reception/view/SizeConfig.dart';
import 'package:digital_reception/view/header.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'model/singletonShareData.dart';
import 'utils.dart';
import 'main.dart';

class QrAccess extends StatefulWidget {

  final bool isEmployee;

  QrAccess({ this.isEmployee });

  @override
  _QrAccessState createState() => _QrAccessState();

}

class _QrAccessState extends State<QrAccess> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  
  bool isError = false;
  int countdown = 3;
  TextEditingController _textController;
  bool isDebug = false;
  Timer timer;
  int textLength = 0;

  @override
  initState() {
    super.initState();


    Utils.startTimer(context: context);
    data.showLoadingUI.listen((bool showLoading) {
      setState(() {
        data.isLoading = showLoading;
      });

    });
    _textController = new TextEditingController();
    Utils.resetTimer();
    timer = Timer.periodic(
        Duration(milliseconds: 500), (timer) {
      data.checkQRCodeScanSuccess(textController: _textController,isEmployee: widget.isEmployee,context: context);
    });
    data.isPushLock = false;
  }

  @override
  void deactivate() {
    super.deactivate();
    timer?.cancel();
    Utils.cancelTimer();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    Utils.cancelTimer();
    super.dispose();
  }

  sendInputRequest({String inputText}) async {
    if (widget.isEmployee) {
     //  inputText = "2151934147";
     // data.scanEmployeeQrCode(context: context,textController: _textController,qrCodeString:inputText);


    } else {
      data.scanQrCode(context: context,textController: _textController,qrCodeJsonString:inputText);


    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: _scaffoldKey,
        body: data.loadingUI(body:GestureDetector(
          onTap: () {
            Utils.resetTimer();
          },
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Header(callback: () {
                  Utils.cancelTimer();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyApp()),
                  );
                }, debugCallback: () {
                  setState(() {
                    isDebug = !isDebug;
                  });
                },),
                Stack(
                  children: <Widget>[
                    Container(
                      height: 1080/Dimension.getRatio(context),
                      child: Stack(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 50.0/Dimension.getRatio(context), bottom: 50/Dimension.getRatio(context)),
                                child: Text(
                                  "Check-in with your QR code. ",
                                  style: TextStyle(
                                    fontSize: 42.0/Dimension.getRatio(context),
                                    fontWeight: FontWeight.w500,
                                    color: const Color(0xff333333),
                                  ),),
                              ),
                              Container(
                                  alignment: Alignment(0.0, 0.0),
                                  child: Stack(
                                    children: <Widget>[
                                      (widget.isEmployee) ? Container(alignment: Alignment(0.0, 0.0),child: Image.asset("scan_qr_terms_staff.png", width: 80/108*Dimension.getFullWidth(context))) : Container(alignment: Alignment(0.0, 0.0),child: Image.asset("scan_qr_terms.png", width: 80/108*Dimension.getFullWidth(context)),),
                                      Container(
                                        height: 500/Dimension.getRatio(context),
                                        margin: EdgeInsets.only(left: 600.0/Dimension.getRatio(context), right: 180/Dimension.getRatio(context), top: 180/Dimension.getRatio(context)),
                                        child: Image.asset("qr_effect.gif", width: 25/108*Dimension.getFullWidth(context)),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 200.0/Dimension.getRatio(context), right: 180/Dimension.getRatio(context), top: 140/Dimension.getRatio(context)),
                                        child: Text(
                                          "Please scan a valid QR code",
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 28/Dimension.getRatio(context),
                                              fontWeight: FontWeight.bold,
                                              color: isError ? const Color(0xFFD6322D) : Colors.transparent
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      )
                                    ],
                                  )
                              )
                            ],
                          ),
                       //   data.progressHUD
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 50.0/Dimension.getRatio(context), bottom: 50/Dimension.getRatio(context), left: 0),
                      alignment: Alignment.topCenter,
                      child: isDebug? Text(
                        Utils.timerCount.toString(),
                        style: TextStyle(
                          fontSize: 15.0/Dimension.getRatio(context),
                          fontWeight: FontWeight.w500,
                          color: const Color(0xff333333),
                        ),) : Container(),
                    ),
                  ],
                ),
                Spacer(),
                Flex(
                  direction: Axis.vertical,
                  children: <Widget>[
                    (widget.isEmployee) ? Container(child: Image.asset("transparency.png"),height: 130/Dimension.getRatio(context),) : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Expanded(
                            flex: 5,
                            child: Align(
                                alignment: Alignment.bottomCenter,
                                child: GestureDetector(
                                  child: Image.asset("photo_off.png"),
                                  onTap: () {

                                  },
                                )
                            )
                        ),
                        Expanded(
                            flex: 5,
                            child: Align(
                                alignment: Alignment.bottomCenter,
                                child: GestureDetector(
                                  child: Image.asset("scan_qr_on.png"),
                                  onTap: () {

                                  },
                                )
                            )
                        ),
                      ],
                    ),

                  ],
                ),
                Stack(
                  children: <Widget>[
                    TextField(
                      controller: _textController,
                      autofocus: true,
                      decoration: InputDecoration(
                      ),
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      onChanged: (inputText) async {
                        if (inputText.length > 0) {
                          if (inputText != "") {
                            debugPrint("inputText:$inputText");
                            // sendInputRequest(inputText:'{"eventId":"10f06e65-71f4-45b3-89ba-7082d1ae85fb","guestId":3}');

                          }
                        }
                      },
                    ),
                    Image.asset("footer_two.png",alignment: Alignment.topCenter, width: size.blockSizeHorizontal * 100,height: size.blockSizeVertical * 28 ,fit: BoxFit.cover),

                  ],
                )
              ],
            ),
          ),
        ))
    );
  }

}