import 'package:digital_reception/model/GuestInfo.dart';
import 'package:digital_reception/model/ServerApiManager.dart';
import 'package:digital_reception/view/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'model/singletonShareData.dart';
import 'view/header.dart';
import 'add_guest.dart';
import 'photo_taking.dart';
import 'checkin_complete.dart';
import 'qraccess.dart';
import 'dart:convert' as convert;
import 'model/guestModel.dart';
import 'dart:convert';
import 'utils.dart';
import 'dart:async';
import 'main.dart';
import 'package:sentry/sentry.dart';
import 'package:flutter/services.dart';
//import 'package:progress_hud/progress_hud.dart';

class Guest extends StatefulWidget {
  Guest({ this.isNew });
  final bool isNew;

  _GuestState createState() => _GuestState();

}

class _GuestState extends State<Guest> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String inputText = "";
  bool isNotRegisterGuest;
  bool isAlpha = true;
  bool isCapital = false;
  bool isPopup = false;
  bool isTncOpened = false;
  bool isChecked = false;
  bool validator = false;
  Timer _timer;
  TextEditingController textEditController;
  String responseMsg = "";
  int mIndex = 0;
  bool isDebug = false;
  FocusNode _focus = new FocusNode();
  int currentBaseOffset = 0;
  int offsetIndex = 0;

  @override
  initState() {
    super.initState();
    Utils.startTimer(context: context);
    isNotRegisterGuest = widget.isNew;
    textEditController = TextEditingController();
    textEditController.addListener(() {

      String text = inputText;

      if (text.length >= 40) {
        text = text.substring(0,40);
        currentBaseOffset = textEditController.selection.baseOffset;
        offsetIndex = 0;
      }

      textEditController.value = textEditController.value.copyWith(text: text);

      if (currentBaseOffset != textEditController.selection.baseOffset) {
        currentBaseOffset = textEditController.selection.baseOffset;
        offsetIndex = 0;
      }
      textEditController.selection =  TextSelection.collapsed(offset: currentBaseOffset+offsetIndex);
    });

    _focus.addListener(() {
      if (_focus.hasFocus) {
        setState((){
          isPopup = true;
        });
      }
    });
    Utils.resetTimer();
    data.isPushLock = false;

  }

  startTimer() async {
    _timer = Timer.periodic(const Duration(seconds: 1), (Timer t) async {
      debugPrint(Utils.timerCount.toString());
      setState(() {
        Utils.timerCount -= 1;
      });

      if (Utils.timerCount == 0) {
        CustomHttpClient().sentry.capture(event: Event(message: 'Guest timer redirect', loggerName: "Timer"));
        t.cancel();
        if (context != null) {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MyApp()),
          );
        }
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    if (_timer != null) {
      Utils.cancelTimer();
    }
    super.dispose();
  }

  keyboardAdditionalDetection () {

    Utils.resetTimer();
    if (inputText.length >= 39) {
      setState(() {
        inputText  = inputText.substring(0,39);
      });
    }
  }

  keyboardGoNavigation () async {

    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(inputText)) {
      setState(() {
        validator = true;
      });
      return;
    } else {
      setState(() {
        validator = false;
      });

      GuestInfo guestInfo = await data.checkGuestExist(context: context,email: inputText,textController: textEditController);

      setState(() {
        // isNotRegisterGuest = guestInfo.data.guestInfo == null ? true : false ;
        // isNotRegisterGuest = true;
        isNotRegisterGuest = !guestInfo.data.eventExist;
      });
    }
  }

  keyboardAfterAction(String char) {
    SystemSound.play(SystemSoundType.click);
    setState(() {
      offsetIndex += 1;
    });
    debugPrint(currentBaseOffset.toString());
    if (textEditController.selection.extentOffset >= 0) {
      inputText = inputText.substring(0,offsetIndex+currentBaseOffset-1)+char+inputText.substring(offsetIndex+currentBaseOffset-1,inputText.length);
    } else {
      inputText += char;
    }
    textEditController.notifyListeners();
  }

  Widget _keyboard (BuildContext context) {
    if (isPopup) {

      String img = "keyboard_alpha.png";
      if (!isAlpha) {
        img = "keyboard_number.png";
      } else {
        if (!isCapital) {
          img = "keyboard_alpha.png";
        } else {
          img = "keyboard_alpha_cap.png";
        }
      }

      return Container(
        child: Transform.scale(
          scale: 23/27,
          child: Stack(
            children: <Widget>[
              Image.asset(img, width: 920,),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 8),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "Q" :"q" : "1";
//                      char = "darrendtamhk@gmail.com";
                      keyboardAfterAction(char);

                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 88),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
//                      keyboardGoNavigation();
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "W" :"w" : "2";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 172),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "E" :"e" : "3";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 256),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "R" :"r" : "4";
                      keyboardAfterAction(char);

                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 338),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "T" :"t" : "5";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 422),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "Y" :"y" : "6";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 506),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "U" :"u" : "7";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 590),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "I" :"i" : "8";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 674),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "O" :"o" : "9";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 758),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "P" :"p" : "0";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8, left: 840),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    SystemSound.play(SystemSoundType.click);
                    setState(() {
                      if (currentBaseOffset != textEditController.selection.baseOffset) {
                        currentBaseOffset = textEditController.selection.baseOffset;
                        offsetIndex = 0;
                      }
                    });

                    if (textEditController.selection.extentOffset >= 0) {
                      inputText = inputText.substring(0,currentBaseOffset+offsetIndex-1)+inputText.substring(currentBaseOffset+offsetIndex,inputText.length);
                    } else {
                      inputText = inputText.substring(0,inputText.length-1);
                    }

                    setState(() {
                      offsetIndex -= 1;
                    });

                    textEditController.notifyListeners();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 40),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "A" :"a" : "@";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 124),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "S" :"s" : "#";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 208),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "D" :"d" : "\$";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 290),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "F" :"f" : "&";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 372),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "G" :"g" : "*";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 454),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "H" :"h" : "(";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 536),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "J" :"j" : ")";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 618),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "K" :"k" : "\'";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 700),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "L" :"l" : "\"";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 80, left: 780),
                width: 130,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () async {

                    SystemSound.play(SystemSoundType.click);
                    //navigation
                    await keyboardGoNavigation();
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 8),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      if (isAlpha) {
                        isCapital = !isCapital;
                      } else {
                        keyboardAdditionalDetection();
                        String char = "~";
                        keyboardAfterAction(char);
                      }
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 88),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "Z" :"z" : "%";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 172),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "X" :"x" : "-";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 256),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "C" :"c" : "+";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 338),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "V" :"v" : "=";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 422),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "B" :"b" : "/";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 506),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "N" :"n" : ";";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 590),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? isCapital ? "M" :"m" : ":";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 674),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? "@" : ",";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 758),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = isAlpha ? "." : ".";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 152, left: 840),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      if (isAlpha) {
                        keyboardAdditionalDetection();
                        //special handle .com
                        if (inputText.length < 37) {
                          String char = ".com";
                          keyboardAfterAction(char);
                        }
                      } else {
                        keyboardAdditionalDetection();
                        String char = "\\";
                        keyboardAfterAction(char);
                      }
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 228, left: 8),
                width: 150,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    SystemSound.play(SystemSoundType.click);
                    setState(() {
                      isAlpha = !isAlpha;
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 228, left: 168),
                width: 500,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = " ";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 228, left: 680),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = "_";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 228, left: 760),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    setState(() {
                      keyboardAdditionalDetection();
                      String char = "-";
                      keyboardAfterAction(char);
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 228, left: 840),
                width: 70,
                height: 70,
                child: GestureDetector(
                  child: Container(color: Colors.transparent,),
                  onTap: () {
                    SystemSound.play(SystemSoundType.click);
                    setState(() {
                      isPopup = !isPopup;
                      _focus.unfocus();
                    });
                  },
                ),
              ),
            ],
          ),
        ),);
    } else {
      return Container();
    }
  }

  Widget _footer (BuildContext context) {
    if (isNotRegisterGuest) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Expanded(
              flex: 5,
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                    child: Image.asset("email_button_on.png"),
                    onTap: () {

                    },
                  )
              )
          ),
          Expanded(
              flex: 5,
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                    child: Image.asset("photo_off.png"),
                    onTap: () {

                    },
                  )
              )
          ),
        ],
      );
    } else {
      return InkWell(
        child: Container(
          child: Image.asset("employee_button.png"),
        ),
        onTap: () {
          SystemSound.play(SystemSoundType.click);
          Utils.cancelTimer();
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => QrAccess(isEmployee: true)),
          );
        },
      );
    }
  }

  Widget _body (BuildContext context) {
    if (isNotRegisterGuest) {
      return Container(
        height: 1150/Dimension.getRatio(context),
        child: Column(
          children: <Widget>[
            isNotRegisterGuest ? Container(
              margin: EdgeInsets.only(top: 200.0/ Dimension.getRatio(context), bottom: 50 / Dimension.getRatio(context)),
              child: Text(
                "Welcome guest!",
                style: TextStyle(
                  fontSize: 52.0/ Dimension.getRatio(context),
                  fontWeight: FontWeight.w500,
                  color: const Color(0xff333333),
                ),
                textAlign: TextAlign.center,
              ),
            ) : Container(margin: EdgeInsets.only(top: 200.0/ Dimension.getRatio(context), bottom: 50 / Dimension.getRatio(context)),),
            Container(
              margin: EdgeInsets.only(top: 50.0/ Dimension.getRatio(context), bottom: 200 / Dimension.getRatio(context)),
              child: Text(
                isNotRegisterGuest ?
                "You have not yet completed the online registration. Please register here." :
                "Sorry, we could not find  your registration record.  Please register to contact  our staff for further assistance.",
                style: TextStyle(
                  fontSize: 42.0/ Dimension.getRatio(context),
                  fontWeight: FontWeight.w500,
                  color: const Color(0xff333333),
                ),
                textAlign: TextAlign.center,
              ),
            ),
            InkWell(
              child: Image.asset("register_button.png",width: 30/108*Dimension.getFullWidth(context),),
              onTap: () {
                SystemSound.play(SystemSoundType.click);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddGuest(newGuest: data.currentGuest ,addGuestType:AddGuestType.Guest)),
                );
              },
            )
          ],
        ),
      );
    } else {
      return  Container(
        height: 1100 / Dimension.getRatio(context),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 50.0/ Dimension.getRatio(context), bottom: 50 / Dimension.getRatio(context)),
              child: Text(
                "Please enter your email.",
                style: TextStyle(
                  fontSize: 42.0/ Dimension.getRatio(context),
                  fontWeight: FontWeight.w500,
                  color: const Color(0xff333333),
                ),
                textAlign: TextAlign.center,
              ),
            ),
            GestureDetector(
              child: Container(
                  margin: EdgeInsets.only(top: 50.0/ Dimension.getRatio(context), bottom: 50/ Dimension.getRatio(context)),
                  alignment: Alignment(0.0/ Dimension.getRatio(context), 0.0/ Dimension.getRatio(context)),
                  child: Card(
                      elevation: 2,
                      child: Container(
                        margin: EdgeInsets.only(top: 50.0/ Dimension.getRatio(context), bottom: 50/ Dimension.getRatio(context)),
                        child: Column(
                          children: <Widget>[
                            Stack(
                              children: <Widget>[
                                Container(
                                  alignment: Alignment(0.0/ Dimension.getRatio(context), 0.0/ Dimension.getRatio(context)),
                                  child: Image.asset("email_input.png", width: Dimension.getFullWidth(context) * 0.7129,),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: Dimension.getFullWidth(context) * 0.144, right: Dimension.getFullWidth(context) * 0.144, top: 50/ Dimension.getRatio(context)),
                                  alignment: Alignment.centerLeft,
                                  child: GestureDetector(
                                    onTapDown: (TapDownDetails tap) {
                                      SystemSound.play(SystemSoundType.click);
                                    },
                                    child: Text(
                                      inputText,
                                      style: TextStyle(
                                        fontSize: 35/ Dimension.getRatio(context),
                                        color: Colors.transparent,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 180.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 30/ Dimension.getRatio(context)),
                                  alignment: Alignment.centerLeft,
                                  child: TextField(
                                    focusNode: _focus,
                                    controller: textEditController,
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Colors.transparent),
                                      ),
                                    ),
                                    style: TextStyle(
                                        fontSize: 35/ Dimension.getRatio(context)
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 840.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 85/ Dimension.getRatio(context)),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    inputText.length.toString()+"/40",
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontSize: 14/ Dimension.getRatio(context),
                                        color: const Color(0xFFA7A7A7)
                                    ),
                                    textAlign: TextAlign.right,
                                  ),
                                ),
                                (validator) ? Container(
                                  margin: EdgeInsets.only(left: 180.0/ Dimension.getRatio(context), right: 180/ Dimension.getRatio(context), top: 85/ Dimension.getRatio(context)),
                                  child: Text(
                                    "Please enter a valid email",
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontSize: 14/ Dimension.getRatio(context),
                                        fontWeight: FontWeight.bold,
                                        color: const Color(0xFFD6322D)
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ) : Container()
                              ],
                            ),
                          ],
                        ),
                      )
                  )
              ),
              onTap: () {
                SystemSound.play(SystemSoundType.click);
                setState(() {
                  isPopup = true;
//                    keyboardGoNavigation();
                });
                Utils.resetTimer();
              },
            ),
            _keyboard(context),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: _scaffoldKey,
        body: data.loadingUI(body:GestureDetector(
        onTap: () {
          Utils.resetTimer();
        },
        child: Container(
//              scrollDirection: Axis.vertical,
        child: Stack(
        children: <Widget>[
        Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
        Header(callback: () {
          Utils.cancelTimer();
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MyApp()),
          );
        }, debugCallback: () {

          setState(() {
            isDebug = !isDebug;
          });
        },),
        Stack(
          children: <Widget>[
            _body(context),
            Container(
              margin: EdgeInsets.only(top: 50.0/ Dimension.getRatio(context), bottom: 50/ Dimension.getRatio(context), left: 0),
              alignment: Alignment.topCenter,
              child: (isDebug) ? Text(
                Utils.timerCount.toString(),
                style: TextStyle(
                  fontSize: 15.0/ Dimension.getRatio(context),
                  fontWeight: FontWeight.w500,
                  color: const Color(0xff333333),
                ),) : Container(),
            ),
          ],
        ),
          Spacer(),
        _footer(context),
          Image.asset("footer_two.png",alignment: Alignment.topCenter, width: size.blockSizeHorizontal * 100,height: size.blockSizeVertical * 25 ,fit: BoxFit.cover),


        ],
        ),
      //  data.progressHUD
        ],
        )
        )
        )));
      }

}
