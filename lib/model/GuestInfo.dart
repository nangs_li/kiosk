class GuestInfo {
  int status;
  String message;
  GuestInfoData data;

  GuestInfo({this.status, this.message, this.data});

  GuestInfo.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new GuestInfoData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GuestInfoData {
  List<GuestInfoUserData> guestInfo;
  List<StaffEmailList> staffEmailList;
  bool eventExist;

  GuestInfoData({this.guestInfo, this.staffEmailList,this.eventExist});

  GuestInfoData.fromJson(Map<String, dynamic> json) {
    if (json['guestInfo'] != null) {
      guestInfo = new List<GuestInfoUserData>();
      json['guestInfo'].forEach((v) {
        guestInfo.add(new GuestInfoUserData.fromJson(v));
      });
    }
    if (json['staffEmailList'] != null) {
      staffEmailList = new List<StaffEmailList>();
      json['staffEmailList'].forEach((v) {
        staffEmailList.add(new StaffEmailList.fromJson(v));
      });
      removeNullDataFromList();
    }
    if (json['eventExist'] != null && json['eventExist'] is bool) {
      eventExist = json['eventExist'];
    } else {
      eventExist = false;
    }

  }

  void removeNullDataFromList(){
    List<StaffEmailList> newStaffEmailList = new List<StaffEmailList>();
    for (StaffEmailList staffEmailList in staffEmailList) {
        if(staffEmailList.userFirstName!= null && staffEmailList.userLastName!= null && staffEmailList.userEmail!= null){

          if(!staffEmailList.userEmail.toLowerCase().contains("delete") && !staffEmailList.userLastName.toLowerCase().contains("delete") && !staffEmailList.userFirstName.toLowerCase().contains("delete")){
            newStaffEmailList.add(staffEmailList);
          }

        }

    }
    staffEmailList = newStaffEmailList;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.guestInfo != null) {
      data['guestInfo'] = this.guestInfo.map((v) => v.toJson()).toList();
    }
    if (this.staffEmailList != null) {
      data['staffEmailList'] =
          this.staffEmailList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GuestInfoUserData {
  int id;
  String guestFirstName;
  String guestLastName;
  String guestEmail;
  String guestIconEndpoint;
  String guestUuid;
  bool guestEmailPermission;
  bool guestPrivacyAgreement;
  String guestCompany;
  String guestPurposeOfVisit;

  GuestInfoUserData(
      {this.id,
        this.guestFirstName,
        this.guestLastName,
        this.guestEmail,
        this.guestIconEndpoint,
        this.guestUuid,
        this.guestEmailPermission,
        this.guestPrivacyAgreement,
        this.guestCompany,
        this.guestPurposeOfVisit});

  GuestInfoUserData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    guestFirstName = json['guest_first_name'];
    guestLastName = json['guest_last_name'];
    guestEmail = json['guest_email'];
    guestIconEndpoint = json['guest_icon_endpoint'];
    guestUuid = json['guest_uuid'];
    guestEmailPermission = json['guest_email_permission'];
    guestPrivacyAgreement = json['guest_privacy_agreement'];
    guestCompany = json['guest_company'];
    guestPurposeOfVisit = json['guest_purpose_of_visit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['guest_first_name'] = this.guestFirstName;
    data['guest_last_name'] = this.guestLastName;
    data['guest_email'] = this.guestEmail;
    data['guest_icon_endpoint'] = this.guestIconEndpoint;
    data['guest_uuid'] = this.guestUuid;
    data['guest_email_permission'] = this.guestEmailPermission;
    data['guest_privacy_agreement'] = this.guestPrivacyAgreement;
    data['guest_company'] = this.guestCompany;
    data['guest_purpose_of_visit'] = this.guestPurposeOfVisit;
    return data;
  }
}

class StaffEmailList {
  String userEmail;
  String userFirstName;
  String userLastName;

  StaffEmailList({this.userEmail,this.userFirstName,this.userLastName});

  StaffEmailList.fromJson(Map<String, dynamic> json) {
    userEmail = json['user_email'];
    userFirstName = json['user_first_name'];
    userLastName = json['user_last_name'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_email'] = this.userEmail;
    data['user_first_name'] = this.userFirstName;
    data['user_last_name'] = this.userLastName;
    return data;
  }
}