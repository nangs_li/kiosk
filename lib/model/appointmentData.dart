class AppointmentData {
  int status;
  String message;
  String staffEmail;
  List<AppointmentUserData> data;

  AppointmentData({this.status, this.message, this.data});

  AppointmentData.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    staffEmail = json['staffEmail'];
    if (json['data'] != null) {
      data = new List<AppointmentUserData>();
      json['data'].forEach((v) {
        data.add(new AppointmentUserData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['staffEmail'] = this.staffEmail;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AppointmentUserData {
  int id;
  int usersId;
  String googleApiRefreshToken;
  String googleApiAsyncToken;
  String googleApiAccessToken;
  String googleApiUuid;
  int googleApiId;
  String eventsUuid;
  String eventsStartDate;
  String eventsEndDate;
  String eventsLocation;
  String eventsName;
  String eventsStatus;
  String googleEventsId;
  String eventsCreatedAt;
  String eventsUpdatedAt;
  int eventsId;
  int guestId;
  bool arrived;
  String guestFirstName;
  String guestLastName;
  String guestEmail;
  String guestIconEndpoint;
  String guestUuid;
  bool guestEmailPermission;
  bool guestPrivacyAgreement;
  String guestCompany;
  String guestPurposeOfVisit;

  AppointmentUserData(
      {this.id,
        this.usersId,
        this.googleApiRefreshToken,
        this.googleApiAsyncToken,
        this.googleApiAccessToken,
        this.googleApiUuid,
        this.googleApiId,
        this.eventsUuid,
        this.eventsStartDate,
        this.eventsEndDate,
        this.eventsLocation,
        this.eventsName,
        this.eventsStatus,
        this.googleEventsId,
        this.eventsCreatedAt,
        this.eventsUpdatedAt,
        this.eventsId,
        this.guestId,
        this.arrived,
        this.guestFirstName,
        this.guestLastName,
        this.guestEmail,
        this.guestIconEndpoint,
        this.guestUuid,
        this.guestEmailPermission,
        this.guestPrivacyAgreement,
        this.guestCompany,
        this.guestPurposeOfVisit});

  AppointmentUserData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    usersId = json['users_id'];
    googleApiRefreshToken = json['google_api_refresh_token'];
    googleApiAsyncToken = json['google_api_async_token'];
    googleApiAccessToken = json['google_api_access_token'];
    googleApiUuid = json['google_api_uuid'];
    googleApiId = json['google_api_id'];
    eventsUuid = json['events_uuid'];
    eventsStartDate = json['events_start_date'];
    eventsEndDate = json['events_end_date'];
    eventsLocation = json['events_location'];
    eventsName = json['events_name'];
    eventsStatus = json['events_status'];
    googleEventsId = json['google_events_id'];
    eventsCreatedAt = json['events_created_at'];
    eventsUpdatedAt = json['events_updated_at'];
    eventsId = json['events_id'];
    guestId = json['guest_id'];
    arrived = json['arrived'];
    guestFirstName = json['guest_first_name'] ?? " ";
    guestLastName = json['guest_last_name'] ?? " ";
    guestEmail = json['guest_email'] ?? " ";
    guestIconEndpoint = json['guest_icon_endpoint'];
    guestUuid = json['guest_uuid'];
    guestEmailPermission = json['guest_email_permission'] ?? true;
    guestPrivacyAgreement = json['guest_privacy_agreement'] ?? true;
    guestCompany = json['guest_company'] ?? " ";
    guestPurposeOfVisit = json['guest_purpose_of_visit'] ?? "meeting";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['users_id'] = this.usersId;
    data['google_api_refresh_token'] = this.googleApiRefreshToken;
    data['google_api_async_token'] = this.googleApiAsyncToken;
    data['google_api_access_token'] = this.googleApiAccessToken;
    data['google_api_uuid'] = this.googleApiUuid;
    data['google_api_id'] = this.googleApiId;
    data['events_uuid'] = this.eventsUuid;
    data['events_start_date'] = this.eventsStartDate;
    data['events_end_date'] = this.eventsEndDate;
    data['events_location'] = this.eventsLocation;
    data['events_name'] = this.eventsName;
    data['events_status'] = this.eventsStatus;
    data['google_events_id'] = this.googleEventsId;
    data['events_created_at'] = this.eventsCreatedAt;
    data['events_updated_at'] = this.eventsUpdatedAt;
    data['events_id'] = this.eventsId;
    data['guest_id'] = this.guestId;
    data['arrived'] = this.arrived;
    data['guest_first_name'] = this.guestFirstName;
    data['guest_last_name'] = this.guestLastName;
    data['guest_email'] = this.guestEmail;
    data['guest_icon_endpoint'] = this.guestIconEndpoint;
    data['guest_uuid'] = this.guestUuid;
    data['guest_email_permission'] = this.guestEmailPermission;
    data['guest_privacy_agreement'] = this.guestPrivacyAgreement;
    data['guest_company'] = this.guestCompany;
    data['guest_purpose_of_visit'] = this.guestPurposeOfVisit;
    return data;
  }
}