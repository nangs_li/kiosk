
import 'package:cloud_firestore/cloud_firestore.dart';

class GuestModel {

   String code = "";
   String firstName = "";
   String lastName = "";
   String email = "";
   String purposeOfVisit = "";
   String company = "";
   String photo = "";
   bool ndaSigned = false;
   bool allowMarketing = false;
   String invitee = "";
   String inviteeEmail = "";
   List<int> cameraPhoto;
   int selected = -1;

  GuestModel({this.code, this.firstName, this.lastName, this.email,this.company,this.photo,this.purposeOfVisit,this.inviteeEmail});

  setSelected () {
    this.selected *= -1;
  }

//  setInvitee(String invitee) {
//    this.invitee = invitee;
//  }

//   setInviteeName(String inviteeEmail) {
//     this.inviteeEmail = inviteeEmail;
//   }

  setCompany(String company) {
    this.company = company;
  }

  setPhoto(String base64) {
    this.photo = base64;
  }

  setCameraPhoto (List<int> data) {
    this.cameraPhoto = cameraPhoto;
  }

  setPurposeOfVisit(String pov) {
    this.purposeOfVisit = pov;
  }

  setAllowMarketing(bool flag) {
    this.allowMarketing = flag;
  }

  toModel (dynamic d) {
    this.code = d['code'];
    this.firstName = d['first_name'];
    this.lastName = d['last_name'];
    this.email = d['email'];
    this.purposeOfVisit = d['purpose_of_visit'];
    this.company = d['company'];
    this.ndaSigned = d['nda_signed'];
    this.allowMarketing = d['allow_marketing'];
    this.invitee = d['invitee'];
    this.inviteeEmail = d['invitee_name'];
    this.photo = d['photo'];
  }

  Map<String, dynamic> toJson() => {
    "code": code,
    "first_name": firstName,
    "last_name": lastName,
    "email": email,
    "purpose_of_visit": purposeOfVisit,
    "company": company,
    "nda_signed": true,
    "allow_marketing": allowMarketing,
    "invitee_name": inviteeEmail,
    "invitee": invitee,
    "photo": photo,
  };

  @override
  String toString() {
    return '${this.code} ,  ${this.firstName} ,  ${this.lastName} ,  ${this.email}, ${this.photo}, ${this.selected}';
  }

  GuestModel emptyGuestModel() {
    return GuestModel(code: "", firstName: "", lastName: "", email: "",company: "",purposeOfVisit:"",photo: null,inviteeEmail:"");
  }
}

class Staff {

  String name;
  String email;

  Staff({ this.name, this.email });

}

class Schedule {
  final String start;
  final String end;
  final String offImageUrl;
  final bool isActive;
  final DocumentReference reference;

  Schedule.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map['start'] != null),
        assert(map['end'] != null),
        assert(map['offImageUrl'] != null),
        assert(map['isActive'] != null),
        start = map['start'],
        end = map['end'],
        offImageUrl = map['offImageUrl'],
        isActive = map['isActive'];

  Schedule.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);

}
