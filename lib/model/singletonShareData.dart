import 'dart:async';
import 'dart:convert';
import 'package:digital_reception/checkid.dart';
import 'package:digital_reception/checkin_complete.dart';
import 'package:digital_reception/model/GuestInfo.dart';
import 'package:digital_reception/model/ServerApiManager.dart';
import 'package:digital_reception/model/appointmentData.dart';
import 'package:digital_reception/thank_you.dart';
import 'package:digital_reception/utils.dart';
import 'package:digital_reception/view/SizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:package_info/package_info.dart';
//import 'package:progress_hud/progress_hud.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'guestModel.dart';
import 'package:http/http.dart' as http;



class SingletonShareData {
  static final SingletonShareData _instance = SingletonShareData._internal();

  static const platform = const MethodChannel('samples.flutter.dev/battery');

  // Get battery level.
  String _batteryLevel = 'Unknown battery level.';
  bool lockQRCodeScanner = false;
  GuestModel currentGuest;
  final BehaviorSubject<AppointmentData> refreshCheckIdPage = BehaviorSubject<AppointmentData>();
  final BehaviorSubject<bool> showLoadingUI = BehaviorSubject<bool>();

  factory SingletonShareData() {
    return _instance;
  }

  List<List<dynamic>> maintenanceExcelList = [];

  SingletonShareData._internal() {
    // init things inside this
  }

  Future<bool> setLabelContext() async {
    String photo;
    bool result = true;
    if(currentGuest.photo != null){
      http.Response response = await http.get(
        currentGuest.photo,
      );
      photo = base64.encode(response.bodyBytes);
    } else {
      photo = currentGuest.photo;
    }
    try {
      int printCount = await data.getPrinterCount();
      bool success = await data.setPrinterCount(printerCount: printCount-1);
      bool result = await platform.invokeMethod('setLabelContext',{"name":"${currentGuest.firstName} ${currentGuest.lastName}","company":currentGuest.company,"photo":photo});
      print("setLabelContext:$result");
    } on PlatformException catch (e) {
      print("Error:${e.message}");
    }

    return result;
  }

  Future<bool> printLabel() async {
    bool result = true;
    try {

      bool result = await platform.invokeMethod('printLabel');
      print("printLabel:$result");
    } on PlatformException catch (e) {
      print("Error:${e.message}");
    }

    return result;
  }

  Future<bool>clearPrintFile() async {
    bool result = true;
    try {
      bool result = await platform.invokeMethod('clearPrintFile');

      print("clearPrintFile:$result");
    } on PlatformException catch (e) {
      print("Error:${e.message}");
    }

    return result;
  }

  bool isValueJson({String jsonString}) {
    Map<String,dynamic> decodedJSON;
    bool isValueJson = false;
    try {
      decodedJSON = json.decode(jsonString) as Map<String, dynamic>;
      isValueJson = true;
    } on FormatException catch (e) {
      print('The provided string is not valid JSON');
      isValueJson = false;
    }

    return isValueJson;
  }


  bool isPushLock = false;
  AppointmentData appointmentData;
  bool isLoading = false;

  GuestInfo guestInfo;
  int textLength;

  Future<void> scanEmployeeQrCode({BuildContext context,TextEditingController textController,String qrCodeString}) async {
    showLoading();
    appointmentData = await serverApi.getAppointmentDataApi(qrCodeString: qrCodeString);
    hideLoading(later: false);
    textController.clear();
    if (appointmentData.status == 200) {
      Utils.cancelTimer();
      AppointmentUserData appointmentUserData = appointmentData.data.last;
      setCurrentGuest(appointmentUserData: appointmentUserData);
        textController.clear();
        await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CheckId())
        );
        if (data.appointmentData.data != null) {
          if (data.appointmentData.data.length > 0) {
            refreshCheckIdPage.sink.add(data.appointmentData);
          }
        }

    } else if (appointmentData.status == 404) {
      showInvalidateQRCodeMessage(context: context,controller: textController);
    }
  }


  Future<void> scanQrCode({BuildContext context,TextEditingController textController,String qrCodeJsonString}) async {
    if (isValueJson(jsonString: qrCodeJsonString)) {
      showLoading();
    AppointmentData appointmentData = await serverApi.appointmentPushNotification(qrCodeJsonString: qrCodeJsonString);
      textController.clear();
    if(appointmentData.status == 200) {
      AppointmentUserData appointmentUserData =  appointmentData.data.last;

      setCurrentGuest(appointmentUserData: appointmentUserData);
      hideLoading(later: true);

        hideLoading(later: false);
        Utils.cancelTimer();
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CheckInComplete(guests: [currentGuest]))
        );


    } else if (appointmentData.status == 404) {
      showInvalidateQRCodeMessage(context: context,controller: textController);
    }
  }
  }

  Future<GuestInfo> checkGuestExist({BuildContext context,String email,TextEditingController textController}) async {
     guestInfo = await serverApi.checkGuestExist(email: email);
     textController.clear();
      if(guestInfo.status == 200) {
        currentGuest = GuestModel().emptyGuestModel();

        if (guestInfo.data.eventExist) {
          GuestInfoUserData guestInfoUserData = guestInfo.data.guestInfo.last;
          currentGuest = GuestModel(
              code: "",
              firstName: guestInfoUserData.guestFirstName ?? "",
              lastName: guestInfoUserData.guestLastName ?? "",
              email: guestInfoUserData.guestEmail ?? "",
              company: guestInfoUserData.guestCompany ?? "",
              photo: guestInfoUserData.guestIconEndpoint ?? null,
              purposeOfVisit: guestInfoUserData.guestPurposeOfVisit ?? ""
          );

          hideLoading(later: true);

          if (!isPushLock) {
            isPushLock = true;
            hideLoading(later: false);
            Utils.cancelTimer();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        CheckInComplete(guests: [currentGuest]))
            );

          }
        } else {
          hideLoading(later: false);
        }
      } else {
        hideLoading(later: false);
      }
    return guestInfo;
  }

  Future<AppointmentData> insertGuestInfo({BuildContext context,GuestModel guestModel,AppointmentData alreadyScanEmployeeQRCode}) async {

    showLoading();
    AppointmentData appointmentData = await serverApi.insertGuestInfo(
        guestModel: guestModel);
    if (appointmentData.status == 200) {
        hideLoading(later: true);

        if (!isPushLock) {
          isPushLock = true;
         hideLoading(later: false);
          Utils.cancelTimer();
          if(alreadyScanEmployeeQRCode == null){
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ThankYou()));
          } else {
            data.appointmentData = appointmentData;
            data.appointmentData.staffEmail = alreadyScanEmployeeQRCode.staffEmail;
            refreshCheckIdPage.sink.add(data.appointmentData);
            Navigator.pop(context);
//            int count = 0;
//            Navigator.of(context).popUntil((_) => count++ >= 2);
          }
        }
      } else if (appointmentData.status == 201 ){
     await insertGuestInfo(context: context,guestModel: guestModel,alreadyScanEmployeeQRCode: alreadyScanEmployeeQRCode);
    } else if(appointmentData.status == 404) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CupertinoAlertDialog(
              title: new Text("Invitee not exist"),
              actions: <Widget>[
                CupertinoDialogAction(
                  child: Text("OK"),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            );
          }
      );
    }
    hideLoading(later: false);
    return appointmentData;
  }

  Future<void> sendRSVP({BuildContext context,AppointmentData appointmentData,List<bool> flagList}) async {

      showLoading();
      List<GuestModel> modelMap = new List();
      for(int i=0;i<appointmentData.data.length;i++) {
        if (flagList[i]) {
          AppointmentUserData appointmentUserData = appointmentData.data[i];
          GuestModel currentGuest = GuestModel(
              code: "",
              firstName: appointmentUserData.guestFirstName ?? "",
              lastName: appointmentUserData.guestLastName ?? "",
              email: appointmentUserData.guestEmail ?? "",
              company: appointmentUserData.guestCompany ?? "",
              photo: appointmentUserData.guestIconEndpoint ?? null,
              purposeOfVisit: appointmentUserData.guestPurposeOfVisit ?? ""
          );
          modelMap.add(currentGuest);
          AppointmentData responseAppointmentData = await serverApi.sendRSVP(userID: appointmentUserData.usersId,guestID: appointmentUserData.guestId);
        }
      }
      hideLoading(later: false);
      if(modelMap.length > 0){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => CheckInComplete(guests: modelMap)),
        );
      }

  }

  void setCurrentGuest({AppointmentUserData appointmentUserData}) {
    currentGuest = GuestModel(
        code: "",
        firstName: appointmentUserData.guestFirstName ?? "",
        lastName: appointmentUserData.guestLastName ?? "",
        email: appointmentUserData.guestEmail ?? "",
        company: appointmentUserData.guestCompany ?? "",
        photo: appointmentUserData.guestIconEndpoint ?? null,
        purposeOfVisit: appointmentUserData.guestPurposeOfVisit ?? ""
    );
  }

  ModalProgressHUD loadingUI({Widget body}){
    return ModalProgressHUD(
      inAsyncCall: data.isLoading,
      child: body,
      opacity: 1.0,
      color: Colors.transparent,
      progressIndicator: CircularProgressIndicator(),
      dismissible: true,
    );
  }


  void showLoading(){
    showLoadingUI.sink.add(true);
    hideLoading(later: true);
  }


  void hideLoading({bool later}) {
    if(later){
      Future.delayed(const Duration(milliseconds: 6000), () {
        showLoadingUI.sink.add(false);
        isPushLock = false;
      });
    } else {
      showLoadingUI.sink.add(false);
    }

  }

  Image bottomImage({String imageName}){
    return Image.asset(imageName,alignment: Alignment.topCenter, width: size.blockSizeHorizontal * 100 ,fit: BoxFit.cover);
  }

  Future<String> getVersion() async{
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    String appName = packageInfo.appName;
    String packageName = packageInfo.packageName;
    String version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;
    print("appName: $appName");
    print("packageName $packageName");
    print("version $version");
    print("buildNumber $buildNumber");
    return version;

  }

  Future<int> getPrinterCount() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int printCount = (prefs.getInt('counter') ?? 300);
    printCount = printCount < 0 ? 0 : printCount;
    return printCount;
  }

  Future<bool> setPrinterCount({int printerCount}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setInt('counter', printerCount);
  }

  void showInvalidateQRCodeMessage({BuildContext context,TextEditingController controller}) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: new Text(
                "the QR Code is invalid, please try again after 15 seconds"),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text("OK"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        }
    );
    controller.clear();
    textLength = 0;
    isPushLock = false;
    hideLoading(later: false);
  }

  checkQRCodeScanSuccess({TextEditingController textController, BuildContext context,bool isEmployee}) {
    String qrCodeString = textController.text;
//    debugPrint("checkQRCodeScanSuccess");
    if(qrCodeString.length!=0 && textLength != qrCodeString.length){
      textLength = qrCodeString.length;
      debugPrint("timerInputText:${qrCodeString}");
      print("textLength$textLength");
    }else if(textController.text.length > 0 &&!isPushLock){

      if(textLength == qrCodeString.length){
        print("textLength$textLength");
        print("_qrCodeString:$qrCodeString");
        if(isEmployee){
          if(isNumeric(qrCodeString)){
            isPushLock = true;
            scanEmployeeQrCode(context: context,textController: textController,qrCodeString:qrCodeString);
          } else {
            showInvalidateQRCodeMessage(context: context,controller: textController);
          }
        } else {
          if(qrCodeString.runtimeType == String&&!isNumeric(qrCodeString)){
            if (data.isValueJson(jsonString: qrCodeString)) {
              isPushLock = true;
              scanQrCode(context: context,textController: textController,qrCodeJsonString:qrCodeString);
            } else {
              showInvalidateQRCodeMessage(context: context,controller: textController);
            }
          } else {
            showInvalidateQRCodeMessage(context: context,controller: textController);
          }
        }
      } else {
        print("textLength != qrCodeString.length");
      }
    }
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }
}


SingletonShareData data = SingletonShareData();