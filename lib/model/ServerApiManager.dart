import 'dart:async';
import 'package:digital_reception/model/GuestInfo.dart';
import 'package:digital_reception/model/appointmentData.dart';
import 'package:digital_reception/model/guestModel.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
final String endpoint = "https://expcms.rxroche.com";
//final String endpoint = "https://api.echo-roaster.com";
final String appointmentEndPoint = endpoint+"/appointment/api";
class ServerApiManager {



  static final ServerApiManager _instance = ServerApiManager._internal();

  factory ServerApiManager() {
    return _instance;
  }

  ServerApiManager._internal() {
    // init things inside this

  }

  Future<Map<String, String>> getUserHeaders() async {
    String jwtToken = await getJwtToken();
    return {
      "x-access-token": jwtToken,
    };
  }

  Future<String> getJwtToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String jwtToken = prefs.getString('jwtToken');
    //jwtToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjF9.XlM78GAV0bm8YzDQWdUKqst5tVUXF6MTSm35aQ5nGsc";
    //jwtToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjJ9.WgDOCAPCxyRNuPQCsVN7hj8t_IPGzfqBndak35QmvXQ";
    //jwtToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjN9.bkiyUEvGo1P4Hp9klBBiO_C-9KRqZfsRuh2yHv7O6E0";
    print("jwtToken:$jwtToken");
    return jwtToken;
  }

  Future<AppointmentData> appointmentPushNotification({String qrCodeJsonString}) async {
    AppointmentData appointmentData;
//    var inputBody = json.encode(qrCodeJsonString);
    //Map<String, String> userHeaders = await getUserHeaders();
    final response = await http.post(appointmentEndPoint+'/pushNotification',body: qrCodeJsonString,headers: {'Content-Type': 'application/json'});
    var body = response.body;
    var bodyJson = jsonDecode(body);

    appointmentData = AppointmentData.fromJson(bodyJson);

    return appointmentData;
  }

  Future<AppointmentData> getAppointmentDataApi({String qrCodeString}) async {
    AppointmentData appointmentData;
    //Map<String, String> userHeaders = await getUserHeaders();
    final response = await http.get(appointmentEndPoint+ '/staff/'+qrCodeString);
    var body = response.body;
    var bodyJson = jsonDecode(body);

    appointmentData = AppointmentData.fromJson(bodyJson);

    return appointmentData;
  }


  Future<AppointmentData> insertGuestInfo({GuestModel guestModel}) async {
    AppointmentData appointmentData;
    var inputBody = json.encode({
      "firstName":guestModel.firstName,
      "lastName":guestModel.lastName,
      "email": guestModel.email,
      "emailPermission":true,
      "privacyAgreement":true,
      "company":guestModel.company,
      "staffEmail":guestModel.inviteeEmail,
      "purposeOfVisit":guestModel.purposeOfVisit,
      "needStaffList": true
    });
    //Map<String, String> userHeaders = await getUserHeaders();
    final response = await http.post(appointmentEndPoint+'/guest',body: inputBody,headers: {'Content-Type': 'application/json'});
    var body = response.body;
    var bodyJson = jsonDecode(body);

    appointmentData = AppointmentData.fromJson(bodyJson);

    return appointmentData;
  }

  Future<AppointmentData> sendRSVP({int userID,int guestID}) async {
    AppointmentData appointmentData;
    var inputBody = json
        .encode({"userID": userID.toString(), "guestID": guestID.toString()});
    //Map<String, String> userHeaders = await getUserHeaders();
    final response = await http.post(appointmentEndPoint+'/sendRSVP',body: inputBody,headers: {'Content-Type': 'application/json'});
    var body = response.body;
    var bodyJson = jsonDecode(body);

    appointmentData = AppointmentData.fromJson(bodyJson);

    return appointmentData;
  }

  Future<bool> labelAlreadyFinish() async {
    bool success = false;
    var inputBody = json
        .encode({"userId": [166], "title": "Reception Kiosk Issue","message":"The printing labels are almost out of stock. Please refill labels.","schedule":false,"scheduleDate":"2026-10-26T07:40:00.000Z","messageTypeId":1});
    //Map<String, String> userHeaders = await getUserHeaders();
    final response = await http.post(endpoint+'/cms/v2/notification',body: inputBody,headers: {'Content-Type': 'application/json'});
    var body = response.body;
    var bodyJson = jsonDecode(body);
    if(bodyJson['status'] == 201){
       success = true;
    }

    return success;
  }

  Future<GuestInfo> checkGuestExist({String email}) async {
    GuestInfo guestInfo;
    //Map<String, String> userHeaders = await getUserHeaders();
    final response = await http.get(appointmentEndPoint+'/user/schedules/email/'+email);
    var body = response.body;
    var bodyJson = jsonDecode(body);

    guestInfo = GuestInfo.fromJson(bodyJson);

    return guestInfo;
  }


  bool checkSuccessStatus({Map data}) {
    bool success = false;
    if(data != null && data is Map){
      if(data['statusCode'] == 202 || data['statusCode'] == 201 || data['statusCode'] == 200) {
        success = true;
      }
      if(data['status'] != null){
        if(data['status'] == 204||data['status'] == 203||data['status'] == 202|| data['status'] == 201 || data['status'] == 200){
          success = true;
        }
      }
    }
    return success;
  }
}

ServerApiManager serverApi = ServerApiManager();