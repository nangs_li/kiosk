import 'package:digital_reception/view/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'model/singletonShareData.dart';
import 'view/header.dart';
import 'dart:ui';
import 'main.dart';
import 'package:flutter/rendering.dart';
import 'utils.dart';

class ThankYou extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _ThankYouState();
  }

}
class _ThankYouState extends State<ThankYou> {

  @override
  initState() {
    super.initState();
    Utils.startTimer(context: context);
    data.isPushLock = false;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Scaffold(
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Header(callback: () {
                Utils.cancelTimer();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyApp()),
                );
              }, debugCallback: () {

              },),
              Container(
                height: 1180/Dimension.getRatio(context),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 200.0/Dimension.getRatio(context), bottom: 50/Dimension.getRatio(context)),
                      child: Text(
                        "Thank you for your registration.",
                        style: TextStyle(
                          fontSize: 42.0/Dimension.getRatio(context),
                          fontWeight: FontWeight.w500,
                          color: const Color(0xff333333),
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 50.0/Dimension.getRatio(context), bottom: 100/Dimension.getRatio(context)),
                      child: Text(
                        "Our staff will be in touch  with you soon.",
                        style: TextStyle(
                          fontSize: 42.0/Dimension.getRatio(context),
                          fontWeight: FontWeight.w500,
                          color: const Color(0xff333333),
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Image.asset("footer_four.png",alignment: Alignment.topCenter, width: size.blockSizeHorizontal * 100,height: size.blockSizeVertical * 25 ,fit: BoxFit.cover),
            ],
          ),
        )
    );
  }

}
