import 'package:digital_reception/view/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'view/checkin_button.dart';
import 'view/header.dart';
import 'guest.dart';
import 'checkin_complete.dart';

class Walkin extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Header(),
              Container(
                height: 800,
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(top: 100.0, bottom: 100),
                      child: Text(
                        "What type of guest are you?",
                        style: TextStyle(
                          fontSize: 42.0,
                          fontWeight: FontWeight.w500,
                          color: const Color(0xff333333),
                        ),),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                          child: CustomButton(resPath: "newguest_button.png"),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Guest(isNew: true)),
                            );
                          },
                        ),
                        GestureDetector(
                          child: CustomButton(resPath: "returnguest_button.png"),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Guest(isNew: false)),
                            );
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
              Spacer(),
              Flex(
                direction: Axis.vertical,
                children: <Widget>[
                  GestureDetector(
                    child: Image.asset("employee_button.png"),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => CheckInComplete(guests: [],)),
                      );
                    },
                  ),
                  Image.asset("footer_two.png",alignment: Alignment.topCenter, width: size.blockSizeHorizontal * 100,height: size.blockSizeVertical * 31 ,fit: BoxFit.cover),
                ],
              ),
            ],
          ),
        )
    );
  }

}
