import 'package:digital_reception/model/singletonShareData.dart';
import 'package:digital_reception/view/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'view/header.dart';
import 'package:flutter/services.dart';
import 'dart:ui';
import 'package:flutter/rendering.dart';
import 'package:camera/camera.dart';
import 'model/guestModel.dart';
import 'main.dart';
import 'utils.dart';

class CheckInComplete extends StatefulWidget {

  final List<GuestModel> guests;

  CheckInComplete({this.guests});

  @override
  State<StatefulWidget> createState() {
    return _CheckInCompleteState(guests: guests);
  }

}
class _CheckInCompleteState extends State<CheckInComplete> {

  _CheckInCompleteState({this.guests});

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  List<CameraDescription> cameras;
  CameraController controller;
  bool isReady = false;
  bool showCamera = true;
  String imagePath;
  List<GuestModel> guests;
  bool isChecked = false;
  bool isTncOpened = false;
  String tncData = "";
  bool sent = false;
  bool isLocked = false;
  List devices = [];
  bool connected = false;
  bool isDebug = false;

  @override
  initState() {
    super.initState();
    Utils.startTimer(context: context);
    data.isPushLock = false;
    printLabelMethod();
    data.isPushLock = false;
  }

  final GlobalKey<State<StatefulWidget>> previewContainer = GlobalKey();

  printLabelMethod() async {
    print("printLabelMethod");
    if(!data.isPushLock){
      print("data.isPushLock");
      data.showLoading();
      data.isPushLock = true;
      bool finishClearPrintFile = await data.clearPrintFile();
      print("finishClearPrintFile:$finishClearPrintFile");

      for ( GuestModel guestModel in guests){
       // print("guestModel:${guestModel.firstName}");
        data.currentGuest = guestModel;
        bool setLabelContext = await data.setLabelContext();
        print("finishSetLabelContext:$setLabelContext");
//        await new Future.delayed( Duration(seconds : 3));
      }
      bool finishPrintLabel = await data.printLabel();
      print("finishPrintLabel:$finishPrintLabel");
      print("data.hideLoading");
        data.hideLoading(later: false);
      Utils.startTimer(context: context);
    }

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: data.loadingUI(body:
        Container(
//          scrollDirection: Axis.vertical,
          child: Stack(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Header(callback: () {
                    Utils.cancelTimer();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyApp()),
                    );
                  }, debugCallback: () {
                    setState(() {
                      isDebug = !isDebug;
                    });
                  },),
                  Container(
                    height: 1100/Dimension.getRatio(context),
                    child: Stack(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Stack(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(top: 50.0/Dimension.getRatio(context), bottom: 50/Dimension.getRatio(context)),
                                  alignment: Alignment(0.0, 0.0),
                                  child: Text(
                                    "Check-in is completed, thank you!",
                                    style: TextStyle(
                                      fontSize: 42.0/Dimension.getRatio(context),
                                      fontWeight: FontWeight.w500,
                                      color: const Color(0xff333333),
                                    ),),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 50.0/Dimension.getRatio(context), bottom: 50/Dimension.getRatio(context), left: 800/Dimension.getRatio(context)),
                                  alignment: Alignment(0.0, 0.0),
                                  child: isDebug ? Text(
                                    Utils.timerCount.toString(),
                                    style: TextStyle(
                                      fontSize: 15.0/Dimension.getRatio(context),
                                      fontWeight: FontWeight.w500,
                                      color: const Color(0xff333333),
                                    ),) : Container(),
                                ),
                              ],
                            ),
                            Card(
                              elevation: 0,
                              child: GestureDetector(
                                child: Container(
                                    margin: EdgeInsets.only(left: 10.0/Dimension.getRatio(context), right: 10/Dimension.getRatio(context)),
                                    child: Container(
                                      alignment: Alignment(0.0, 0.0),
                                      child: RepaintBoundary(
                                          key: previewContainer,
                                          child: Stack(
                                            children: <Widget>[
                                              Container(alignment: Alignment(0.0, 0.0),child: Image.asset("complete_print.png", width: 88/108*Dimension.getFullWidth(context),)),
                                              Container(
                                                height: 500/Dimension.getRatio(context),
                                                margin: EdgeInsets.only(left: 600.0/Dimension.getRatio(context), right: 110/Dimension.getRatio(context), top: 300/Dimension.getRatio(context)),
                                                child: Image.asset("printer.gif", width: 25/108*Dimension.getFullWidth(context)),
                                              )
                                            ],
                                          )
                                      ),
                                    )
                                ),
                                onTap: () {
//                            _printScreen();
                                },
                              ),
                            )
                          ],
                        ),
                       // data.progressHUD
                      ],
                    ),
                  ),
                  Spacer(),
                  Flex(
                    direction: Axis.vertical,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              flex: 5,
                              child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: InkWell(
                                    child: isLocked ? Image.asset("print_again_disable.png") : Image.asset("print_again.png"),
                                    onTap: () async {
                                      if (!isLocked) {
                                        SystemSound.play(SystemSoundType.click);
                                        printLabelMethod();
                                      }
                                    },
                                  )
                              )
                          ),
                        ],
                      ),
                      Image.asset("footer_three.png",alignment: Alignment.topCenter, width: size.blockSizeHorizontal * 100,height: size.blockSizeVertical * 20 ,fit: BoxFit.cover),
                    ],
                  ),
                ],
              ),
            ],
          )
        ))
    );
  }

}
